<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmailPelanggaran extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
 // return $this->from('pengirim@malasngoding.com')
 //        ->view('email')
 //        ->attach('../public/file_email/test.pdf');
 //        // return $this->view('view.name');

        return $this->from('PPK.Poltekpel.Surabaya@sitapos.com')->subject('Surat Panggilan')
        ->view('email')
        ->attach(public_path('/file_email').'/surat_panggilan.pdf', [
         'as' => 'surat_panggilan.pdf',
         'mime' => 'application/pdf',
     ]);
        // return $this->view('view.name');
    }
}
