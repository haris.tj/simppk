<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\jasmaniModel;
use DataTables;

class PrestasiController extends Controller{
	public function home($id){
		$data = array(
			'user' => DB::table('data_tarunas')->where('id_user',$id)->first(),
			'prestasi' => DB::table('data_prestasi')->where('id_user',$id)->orderBy('id', 'DESC')->get(),
			'pelanggaran' => DB::table('tb_pelanggaran')
			->select('tb_pelanggaran.tingkat','tb_pelanggaran.jenis_pelanggaran','tb_pelanggaran.tahap_orientasi','tb_pelanggaran.tahap_pembentukan','tb_pelanggaran.tahap_pendewasaan','tb_pelanggaran.tahap_pematangan','data_pelanggrans.id')
			->join('data_pelanggrans','data_pelanggrans.id_pelanggaran','=','tb_pelanggaran.id_pelanggaran')
			->where('data_pelanggrans.id_user',$id)
			->get(),
			'sam' => DB::table('data_pelanggrans')
			->select(
				DB::raw('sum(orientasi) as orientasi'),
				DB::raw('sum(pembentukan) as pembentukan'),
				DB::raw('sum(pendewasaan) as pendewasaan'),
				DB::raw('sum(pematangan) as pematangan'),
			)->where('id_user',$id)->first(),
		);
		return view('admin.user.prestasi_home',$data);
	}
	public function getPrestasi($id){
		$data = DB::table('tb_prestasi')->where('bidang',$id)->get();
		echo json_encode($data);
	}
	public function getDetPrestasi($id){
		$data = DB::table('tb_prestasi')->where('id_pres',$id)->first();
		echo json_encode($data);
	}
	public function insert(Request $request){
		$data = array(
			'id_user' => $request->id_user,
			'id_prestasi' => $request->id_prests,
			'bidang'	=> $request->bidang,
			'prestasi' => $request->prestasi,
			'poin' => $request->poin,
			'semester' => $request->semester,
			'waktu'	=> $request->tgl,
		);
		if (DB::table('data_prestasi')->insert($data)) {
			return redirect()->back()->with('success','Insert Data Berhasil');
		}
	}
	public function delete($id){
		if (Auth::User()->role == 'super admin') {
			$del = DB::table('data_prestasi')->where('id',$id)->delete();
			if ($del) {
				return redirect()->back()->with('success','Delete Data Berhasil');
			}
		}
		abort(404,'Page not found');
	}
	public function getData($id){
		$data = DB::table('data_prestasi')->where('id',$id)->first();
		echo json_encode($data);
	}
	public function update(Request $request){
		$data = array(
			'id_prestasi' => $request->id_prests,
			'bidang'	=> $request->bidang,
			'prestasi' => $request->prestasi,
			'poin' => $request->poin,
			'semester' => $request->semester,
			'waktu'	=> $request->tgl,
		);
		if (DB::table('data_prestasi')->where('id',$request->id_pr)->update($data)) {
			return redirect()->back()->with('success','Update Data Berhasil');
		}
	}
	public function jasmaniJson($id){
		return DataTables::of(jasmaniModel::where('id_user',$id))->addIndexColumn()->addColumn('action',function($row){

			$btn_sadmin = 
			'<button class="btn btn-sm btn-rounded btn-danger" onclick="hapus('.$row->id.')">Hapus</button>
			<button class="btn btn-sm btn-rounded btn-warning" onclick="ubah('.$row->id.')">Ubah</button>';
			$btn_admin = 
			'<button class="btn btn-sm btn-rounded btn-warning" onclick="ubah('.$row->id.')">Ubah</button>';
			return Auth::User()->role == "super admin" ? $btn_sadmin : $btn_admin; 
		})->rawColumns(['action'])
		->make(true);
	}

	public function insertJasmani(Request $request){
		$js = new jasmaniModel;
		$js->id_user = $request->id_user;
		$js->pusup = $request->pushup;
		$js->situp = $request->situp;
		$js->lari = $request->lari;
		$js->lompat = $request->lompat;

		if ($js->save()) {
			$status = 200;
			$message = "Tambah Data Berhasil";
		}else{
			$status = 400;
			$message = "Gagal Menambahkan Data";
		}
		return json_encode([
			'status'	=> $status,
			'message'	=> $message,
			'data'		=> $js
		]);
	}
	public function hapusJasmani(Request $request){
		$js = jasmaniModel::find($request->id);
		if ($js->delete()) {
			$status = 200;
			$message = "Hapus Data Berhasil";
		}else{
			$status = 400;
			$message = "Gagal Menghapus Data";
		}
		return json_encode([
			'status'	=> $status,
			'message'	=> $message,
			'data'		=> $js
		]);
	}
	public function updateJasmani(Request $request){
		$js = jasmaniModel::find($request->id);
		$js->id_user = $request->id_user;
		$js->pusup = $request->pushup;
		$js->situp = $request->situp;
		$js->lari = $request->lari;
		$js->lompat = $request->lompat;
		if ($js->update()) {
			$status = 200;
			$message = "Ubah Data Berhasil";
		}else{
			$status = 400;
			$message = "Gagal Mengubah Data";
		}
		return json_encode([
			'status'	=> $status,
			'message'	=> $message,
			'data'		=> $js
		]);
	}
	public function getJasmani($id){
		return json_encode(jasmaniModel::find($id));
	}
}
