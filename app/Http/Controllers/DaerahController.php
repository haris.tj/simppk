<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DaerahController extends Controller
{
    //
	public function provinsi(){
		$prov = DB::table('wilayah_provinsi')->get();
		echo json_encode($prov);
	}
	public function kabupaten($id){
		$kab = DB::table('wilayah_kabupaten')->where('provinsi_id',$id)->get();
		echo json_encode($kab);
	}
	public function kecamatan($id){
		$kec = DB::table('wilayah_kecamatan')->where('kabupaten_id',$id)->get();
		echo json_encode($kec);
	}
	public function desa($id){
		$des = DB::table('wilayah_desa')->where('kecamatan_id',$id)->get();
		echo json_encode($des);
	}
	public function daerahOrtu($id){
		$or = DB::table('data_orangtuas')->where('id_user',$id)->first();
		$data = array(
			'pro'	=> DB::table('wilayah_provinsi')->where('id',$or->provinsi)->first(),
			'kab'	=> DB::table('wilayah_kabupaten')->where('id',$or->kabupaten)->first(),
			'kec'	=> DB::table('wilayah_kecamatan')->where('id',$or->kecamatan)->first(),
			'des'	=> DB::table('wilayah_desa')->where('id',$or->desa)->first()
		);
		echo json_encode($data);
	}
}
