<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\modelKelas;
use Auth;

class controllerKelas extends Controller
{
    //
	public function index(){
		return view('admin.kelas.index');
	}
	public function json(){
		$data = modelKelas::all();
		if (Auth::User()->role == 'operator') {
			return Datatables::of(modelKelas::where('id_user',Auth::User()->id))
			->addIndexColumn()
			->addColumn('action', function($row){

				$btn = '<button class="btn btn-sm btn-rounded btn-danger" onclick="hapus('.$row->id.')">Hapus</button>
				<button class="btn btn-sm btn-rounded btn-warning" onclick="ubah('.$row->id.')">Ubah</button>';

				return $btn;
			})
			->rawColumns(['action'])
			->make(true);
		}
		return Datatables::of($data)->make(true);
	}
	public function insert(Request $request){
		$kls = new modelKelas;
		$kls->id_user = Auth::User()->id;
		$kls->nama_kelas = $request->nama_kelas;
		if ($kls->save()) {
			$status = 200;
			$message = "Tambah Data Kelas Berhasil";
		}else{
			$status = 400;
			$message = "Gagal Menambahkan Kelas";
		}
		return json_encode([
			'status' => $status,
			'message' => $message,
			'data'	=> $kls
		]);
	}

	public function update(Request $request){
		$kls = modelKelas::find($request->id);
		$kls->id_user = Auth::User()->id;
		$kls->nama_kelas = $request->nama_kelas;
		if ($kls->update()) {
			$status = 200;
			$message = "Ubah Data Kelas Berhasil";
		}else{
			$status = 400;
			$message = "Gagal Mengubah Data Kelas";
		}
		return json_encode([
			'status' => $status,
			'message' => $message,
			'data'	=> $kls
		]);
	}

	public function detail($id){
		return json_encode(modelKelas::where('id',$id)->first());
	}
	public function hapus(Request $request){
		$kls = modelKelas::find($request->id);
		if ($kls->delete()) {
			$status = 200;
			$message = "Hapus Data Berhasil";
		}else{
			$status = 400;
			$message = "Gagal Menghapus Data";
		}
		return json_encode([
			'status' => $status,
			'message' => $message,
			'data'	=> $kls
		]);
	}
}
