<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use PDF;

class MasterController extends Controller
{
	public function indexprestasi(){
		if (Auth::User()->role == 'super admin' || Auth::User()->role == 'admin') {
			$data = DB::table('tb_prestasi')->get();
			return view('admin.prestasi.home',['data' => $data]);
		}abort(404,'Page not found');
	}
	public function loadHtml(){
		$view = view('admin.prestasi.insert')->render();
		return response()->json(['html'=>$view]);
	}
	public function insertPrestasi(Request $request){
		$data = array(
			'bidang'	=> $request->bidang,
			'prestasi'	=> $request->prestasi,
			'poin'		=> $request->poin
		);
		if (DB::table('tb_prestasi')->insert($data)) {
			return redirect()->back()->with('success','Tambah Data Berhasil');
		}
	}
	public function deletePrestasi($id){
		if (Auth::User()->role == 'super admin') {
			if (DB::table('tb_prestasi')->where('id_pres',$id)->delete()) {
				return redirect()->back()->with('success','Hapus Data Berhasil');
			}
		}abort(404,'Page not found');
	}
	public function viewUpdatePrestasi($id){
		$data = DB::table('tb_prestasi')->where('id_pres',$id)->first();
		echo json_encode($data);
	}
	public function updatePrestasi(Request $request){
		$data = array(
			'bidang'	=> $request->bidang,
			'prestasi'	=> $request->prestasi,
			'poin'		=> $request->poin
		);
		if (DB::table('tb_prestasi')->where('id_pres',$request->id_prs)->update($data)) {
			return redirect()->back()->with('success','Ubah Data Berhasil');
		}
	}
	public function indexPelanggaran(){
		if (Auth::User()->role == 'super admin' || Auth::User()->role == 'admin') {
			$data = DB::table('tb_pelanggaran')->get();
			return view('admin.new_pelanggaran.home',['data' => $data]);
		}abort(404,'Page not found');
	}
	public function loadHtmlPel(){
		$view = view('admin.new_pelanggaran.insert')->render();
		return response()->json(['html'=>$view]);
	}
	public function insertPelanggaran(Request $request){
		$data = array(
			'tingkat'			=> $request->tingkat,
			'jenis_pelanggaran'	=> $request->pelanggaran,
			'tahap_orientasi'	=> $request->orientasi,
			'tahap_pembentukan'	=> $request->pembentukan,
			'tahap_pendewasaan'	=> $request->pendewasaan,
			'tahap_pematangan'	=> $request->pematangan
		);
		if (DB::table('tb_pelanggaran')->insert($data)) {
			return redirect()->back()->with('success','Tambah Data Berhasil');
		}
	}
	public function viewUpdatePelanggaran($id){
		$data = DB::table('tb_pelanggaran')->where('id_pelanggaran',$id)->first();
		echo json_encode($data);
	}
	public function updatePelanggaran(Request $request){
		$data = array(
			'tingkat'			=> $request->tingkat,
			'jenis_pelanggaran'	=> $request->pelanggaran,
			'tahap_orientasi'	=> $request->orientasi,
			'tahap_pembentukan'	=> $request->pembentukan,
			'tahap_pendewasaan'	=> $request->pendewasaan,
			'tahap_pematangan'	=> $request->pematangan
		);
		if (DB::table('tb_pelanggaran')->where('id_pelanggaran',$request->id_pel)->update($data)) {
			return redirect()->back()->with('success','Ubah Data Berhasil');
		}
	}
	public function deletePelanggaran($id){
		if (Auth::User()->role == 'super admin') {
			if (DB::table('tb_pelanggaran')->where('id_pelanggaran',$id)->delete()) {
				return redirect()->back()->with('success','Hapus Data Berhasil');
			}
		}abort(404,'Page not found');
	}
	public function indexPDF(){
		$data = ['data'=>DB::table('tb_pdf')->where('id',1)->first()];
		return view('pdf_taruna.pdf_manage',$data);
	}
	public function previewPDF(){
		$data = ['data' => DB::table('tb_pdf')->where('id',1)->first()];
		$pdf = PDF::loadview('pdf_taruna',$data);
		return $pdf->stream();
	}
	public function updatePDF(Request $request){
		$data = array(
			'nama'			=> $request->nama_perwira,
			'telepon'		=> $request->tlp_perwira,
			'nama_direktur'	=> $request->nama_direktur,
			'penata'	=> $request->penata,
			'nip'			=> $request->nip,
		);
		if (DB::table('tb_pdf')->where('id',1)->update($data)) {
			return redirect()->back()->with('success','Ubah Data Berhasil');
		}
	}
}
