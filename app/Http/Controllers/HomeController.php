<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;
use Session;
use DB;
use App\Mail\SendEmailPelanggaran;
use Illuminate\Support\Facades\Mail;
use DataOrangtua;
use PDF;
use App\modelKelas;
use App\DataProdi;
use Validator;

class HomeController extends Controller{

    public function index(){
        if (Auth::user()->role == 'super admin') {
            $data = User::all();
            return view('admin.home.home',['data' => $data]);
        }
        return redirect('dashboard');
        // abort(404,'Page not found');
    }

    public function view_update($id){

        if (Auth::user()->role == 'super admin') {
            $data = User::find($id);
            return json_encode($data);
        }
        return redirect()->route('home');
    }

    public function update(Request $request){

        if (Auth::user()->role == 'super admin') {
            $validator = Validator::make($request->all(),[
                'email' => 'required|unique:users,email|email'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput($request->all);
            }

            if (empty($request->password) || $request->password == "") {
                // jika password kosong, maka password tidak akan di update
                $id = $request->id;
                $user = User::find($id);
                $user->name = $request->nama;
                $user->email = $request->email;
                $user->role = $request->role;
                $simpan = $user->update();

                if($simpan){
                    Session::flash('success', 'Update Data Berhasil');
                    return redirect()->route('home');
                } else {
                    Session::flash('error','Update Data Gagal, Coba Lagi.!');
                    return redirect()->route('home');
                }
            }else{
                // jika password != kosong maka password akan di update
                $id = $request->id;
                $user = User::find($id);
                $user->name = $request->nama;
                $user->email = $request->email;
                $user->role = $request->role;
                $user->password = Hash::make($request->password);
                $simpan = $user->update();

                if($simpan){
                    Session::flash('success', 'Update Data Berhasil');
                    return redirect()->route('home');
                } else {
                    Session::flash('error', 'Update Data Gagal, Coba Lagi.!');
                    return redirect()->route('home');
                }
            }

        }
        abort(404,'Page not found');

    }
    public function delete($id){
        if (Auth::user()->role == 'super admin') {
            $user = User::find($id);

            if($user->delete()){
                Session::flash('success', 'Hapus Data Berhasil');
                return redirect()->route('home');
            } else {
                Session::flash('error','Hapus Data Gagal, Coba Lagi.!');
                return redirect()->route('home');
            }
        }
        abort(404,'Page not found');
    }

    public function insert(Request $request){
        if (Auth::user()->role == 'super admin') {
            $validator = Validator::make($request->all(),[
                'email' => 'required|unique:users,email|email',
                'role' => 'required',
                'password' => 'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput($request->all);
            }
            $user = new User;
            $user->name = $request->nama;
            $user->email = $request->email;
            $user->role = $request->role;
            $user->password = Hash::make($request->password);
            if ($user->save()) {
                Session::flash('success', 'Insert Data Berhasil');
                return redirect()->route('home');
            }else{
                Session::flash('error','Insert Data Gagal, Coba Lagi.!');
                return redirect()->route('home');
            }
        }
        abort(404,'Page not found');
    }

    public function dash(){
        $role = Auth::user()->role;
        $usr = count(User::all());
        $kls = count(modelKelas::all());
        $prd = count(DataProdi::all());
        if ($role == 'operator') {
            $vw = DB::table('view_modal_pelanggaran')->where('operator_id',Auth::user()->id)->get();
            
            $data = array(
                'data' => $vw,
                'user'  => $usr, 
                'kls'  => $kls, 
                'prd'  => $prd, 
            );

            return view('home',$data);
        }

        if ($role == "user") {
            return redirect('user/data');
        }
        $data = array(
            'data' => DB::table('view_pelanggaran')->where('orientasi','>=',50)->get(),
            'user'  => $usr, 
            'kls'  => $kls, 
            'prd'  => $prd, 
        );
        return view('home',$data);
    }

    public function sendEmail($id){
       if(Auth::User()->role != 'user') {
                $dt_ortu = DB::table('data_orangtuas')->where('id_user',$id)->first();
                $dt_pel = DB::table('excel_pel')->where('id_user',$id)->first();
                $data = array(
                    'detail'    => DB::table('data_tarunas')->where('id_user',$id)->first(),
                    'ortu'      => $dt_ortu,
                    'pel'       => DB::table('data_tarunas')
                    ->select('data_tarunas.id_user','data_tarunas.nit','detail_pelanggaran.poin_kesalahan','detail_pelanggaran.pelanggaran','detail_pelanggaran.waktu')
                    ->join('detail_pelanggaran','data_tarunas.nit','=','detail_pelanggaran.nit')
                    ->where('data_tarunas.id_user',$id)->first(),
                    'boss'      => DB::table('tb_pdf')->where('id',1)->first()
                );
            $p = $dt_pel->orientasi;
            if ($p >= 50 && $p<75) {
              $pdf = PDF::loadview('pdf_taruna.peringatan_1',$data);
              $path = public_path('file_email/');
              $pdf->save($path . '/' . 'surat_panggilan.pdf');
              // return $pdf->stream();
              }elseif ($p >= 75 && $p<100) {
               $pdf = PDF::loadview('pdf_taruna.peringatan_2',$data);
                          $path = public_path('file_email/');
                          $pdf->save($path . '/' . 'surat_panggilan.pdf');
               // return $pdf->stream();
           }elseif($p >= 100){
               $pdf = PDF::loadview('pdf_taruna.peringatan_3',$data);
               $path = public_path('file_email/');
               $pdf->save($path . '/' . 'surat_panggilan.pdf');
               // return $pdf->stream();
           }
            // sebelum dikiim
           Mail::to($dt_ortu->email_ortu)->send(new SendEmailPelanggaran());
           if (Mail::failures()) {
            return redirect()->back()->with('error',Error(Mail::failures()));
        }
        DB::table('data_pelanggrans')->where('id_user',$id)->update(['status' => 1]);
           return redirect()->back()->with('success','Email Berhasil Dikirimkan ke ('.$dt_ortu->email_ortu.')');
    }
 }
 
}
