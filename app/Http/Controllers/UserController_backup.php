<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\UserModel;
use App\DataTaruna;
use App\DataOrangtua;
use App\DataMediaTaruna;
use App\DataProdi;
use App\User;
use Auth;
use Session;
use Validator;

class DataUserController extends Controller{
    //
	public function index(){
		$id = Auth::User()->id;
		$d1 = DataTaruna::where('id_user',$id)->count();
		$d2 = DataMediaTaruna::where('id_user',$id)->count();
		$d3 = DataOrangtua::where('id_user',$id)->count();
		$role = Auth::User()->role;
		if ($role != 'user') {		
			if ($role == 'operator') {
				$k1 = DataTaruna::where(
					'kelas',Auth::user()->kelas1)
				->orWhere('kelas',Auth::user()->kelas2)
				->get();
				return view('admin.user.home',['data' => $k1]);
			}
			$data = ['data' => DataTaruna::all()];
			return view('admin.user.home',$data);
		}

		if ($d1 != 1) {
			// jika data != 1 maka user harus menginsert data dahulu.
			$data = DB::table('wilayah_provinsi')->get();
			return view('user.profile.home',['data' => $data]);
		}elseif ($d2 != 1) {
			$data = DB::table('wilayah_provinsi')->get();
			return view('user.profile.home',['data' => $data]);
		}elseif ($d3 != 1) {
			$data = DB::table('wilayah_provinsi')->get();
			return view('user.profile.home',['data' => $data]);
		}else{
			// jika data sudah tersedia data akan ditampilkan
			$wil = DataOrangtua::where('id_user',$id)->first();
			$data = array(
				'data1' => DataTaruna::where('id_user',$id)->get(),
				'data2' => DataOrangtua::where('id_user',$id)->get(),
				'data3' => DataMediaTaruna::where('id_user',$id)->get(),
				'prov' => DB::table('wilayah_provinsi')->where('id',$wil->provinsi)->value('nama'),
				'kab' => DB::table('wilayah_kabupaten')->where('id',$wil->kabupaten)->value('nama'),
				'kec' => DB::table('wilayah_kecamatan')->where('id',$wil->kecamatan)->value('nama'),
				'des' => DB::table('wilayah_desa')->where('id',$wil->desa)->value('nama')
			);
			return view('user.profile.view',$data);
		}
		return redirect()->route('dashboard');	
	}
	public function add_ajax_kab($id_prov){
		$query = DB::table('wilayah_kabupaten')->where('provinsi_id',$id_prov)->get();
		$data = "<option value=''>Pilih Kabupaten</option>";
		foreach ($query as $value) {
			$data .= "<option value='".$value->id."'>".$value->nama."</option>";
		}
		echo $data;
	}
	public function add_ajax_kec($id_kab){
		$query = DB::table('wilayah_kecamatan')->where('kabupaten_id',$id_kab)->get();
		$data = "<option value=''>Pilih Kecamatan</option>";
		foreach ($query as $value) {
			$data .= "<option value='".$value->id."'>".$value->nama."</option>";
		}
		echo $data;
	}
	public function add_ajax_des($id_kec){
		$query = DB::table('wilayah_desa')->where('kecamatan_id',$id_kec)->get();
		$data = "<option value=''>Pilih Desa</option>";
		foreach ($query as $value) {
			$data .= "<option value='".$value->id."'>".$value->nama."</option>";
		}
		echo $data;
	}
	public function dataTaruna(Request $request){

		$data = new DataTaruna;
		$jmlh = DataTaruna::where('id_user',Auth::User()->id)->count();

		if ($jmlh >= 1) {
			Session::flash('errors', 'Insert Data Gagal, Data Sudah Tersedia.!');
			return redirect()->back();//->route('dashboard');
		}else{

			$validator = Validator::make($request->all(),[
				'nit'   => 'required|min:10|max:11',
				'kelas' => 'required',
				'semester' => 'required',
			]);
			if($validator->fails()){
				return redirect()->back()->withErrors($validator)->withInput($request->all);
			}
			$data->id_user = Auth::User()->id;
			$data->nama = $request->nama;
			$data->nit = $request->nit;
			// $data->wali = $request->wali;
			$data->tahun = $request->tahun;
			$data->program = $request->program;
			$data->semester = $request->semester;
			$data->kelas = $request->kelas;
			$data->tempatlahir = $request->tempat;
			$data->tgllahir = $request->tgl_lahir;

			// print_r(substr($request->nit, 8,2));
			if ($data->save()) {
				Session::flash('success', 'Insert Data Berhasil');
				return redirect()->back();//->route('dashboard');
			} else {
				Session::flash('errors','Insert Data Gagal, Coba Lagi.!');
				return redirect()->back();//->route('user/data');
			}
		}
	}
	public function dataOrtu(Request $request){
		$data = new DataOrangtua;
		$jmlh = DataOrangtua::where('id_user',Auth::User()->id)->count();

		if ($jmlh >= 1) {
			Session::flash('errors', 'Insert Data Gagal, Data Sudah Tersedia.!');
			return redirect()->back();//->route('dashboard');
		}else{
			$validator = Validator::make($request->all(),[
				'notlportu1'   => 'required|min:11|max:12',
				'notlportu2' => 'required|min:11|max:12',
				'emailortu' => 'required|email',
			]);
			if($validator->fails()){
				return redirect()->back()->withErrors($validator)->withInput($request->all);
			}
			$data->id_user = Auth::User()->id;
			$data->nama_ortu = $request->ortu;
			$data->provinsi = $request->prov;
			$data->kabupaten = $request->kab;
			$data->kecamatan = $request->kec;
			$data->desa = $request->des;
			$data->tlp_ortu1 = $request->notlportu1;
			$data->tlp_ortu2 = $request->notlportu2;
			$data->email_ortu = $request->emailortu;

			if ($data->save()) {
				Session::flash('success', 'Insert Data Berhasil');
				return redirect('user/data');//->route('dashboard');
			} else {
				Session::flash('errors', ['' => 'Insert Data Gagal, Coba Lagi.!']);
				return redirect('user/data');//->route('user/data');
			}
		}
	}
	
	public function dataMediaTaruna(Request $request){
		$jmlh = DataMediaTaruna::where('id_user',Auth::User()->id)->count();

		if ($jmlh >= 1) {
			Session::flash('errors', 'Insert Data Gagal, Data Sudah Tersedia.!');
			return redirect()->back();//->route('dashboard');
		}else{
			$validator = Validator::make($request->all(),[
				'notlptaruna'   => 'required|min:11|max:12',
				'ekstrakurikuler' => 'required',
				'emailtaruna' => 'required|email',
			]);
			if($validator->fails()){
				return redirect()->back()->withErrors($validator)->withInput($request->all);
			}
			$extArr = $request->ekstrakurikuler;
			$data = new DataMediaTaruna;
			$data->id_user = Auth::User()->id;
			$data->tlp_taruna = $request->notlptaruna;
			$data->email_taruna = $request->emailtaruna;
			$data->facebook = $request->facebook;
			$data->instagram = $request->instagram;
			$data->twitter = $request->twitter;
			$data->ektra_kurikuler = implode("|", $extArr);
			$data->staff = $request->staff;

			if ($data->save()) {
				Session::flash('success', 'Insert Data Berhasil');
				return redirect('user/data');//->route('dashboard');
			} else {
				Session::flash('error','Insert Data Gagal, Coba Lagi.!');
				return redirect('user/data');//->route('user/data');
			}
		}
	}
	public function detail($id){
		$wil = DataOrangtua::where('id_user',$id)->first();

		if (Auth::User()->role != 'user') {		
			$cek1 = DataOrangtua::where('id_user',$id)->count();
			$cek2 = DataMediaTaruna::where('id_user',$id)->count();
			// dd($cek1);
			if ($cek1 != 0 || $cek2 != 0 ) {
				$data = array(
					'data' => DataTaruna::all(),
					'data1' => DataTaruna::where('id_user',$id)->get(),
					'data2' => DataOrangtua::where('id_user',$id)->get(),
					'data3' => DataMediaTaruna::where('id_user',$id)->get(),
					'prov' => DB::table('wilayah_provinsi')->where('id',$wil->provinsi)->value('nama'),
					'kab' => DB::table('wilayah_kabupaten')->where('id',$wil->kabupaten)->value('nama'),
					'kec' => DB::table('wilayah_kecamatan')->where('id',$wil->kecamatan)->value('nama'),
					'des' => DB::table('wilayah_desa')->where('id',$wil->desa)->value('nama')
				);
				return view('admin.user.detail',$data);	
			} 
			// Session::flash('errors','Insert Data Gagal, Coba Lagi.!');
			return redirect()->back()->with('error', 'Data User Belum Lengkap');
			// echo "Data belum lengkap";
		}
	}
	public function edit_view($id){
		$wil = DataOrangtua::where('id_user',$id)->first();

		if (Auth::User()->role != 'user') {	
			$cek1 = DataOrangtua::where('id_user',$id)->count();
			$cek2 = DataMediaTaruna::where('id_user',$id)->count();
			// dd($cek1);
			if ($cek1 != 0 || $cek2 != 0 ){
				$data = array(
					'data' => DataTaruna::all(),
					'data1' => DataTaruna::where('id_user',$id)->get(),
					'data2' => DataOrangtua::where('id_user',$id)->get(),
					'data3' => DataMediaTaruna::where('id_user',$id)->get(),
					'prov' => DB::table('wilayah_provinsi')->where('id',$wil->provinsi)->value('nama'),
					'kab' => DB::table('wilayah_kabupaten')->where('id',$wil->kabupaten)->value('nama'),
					'kec' => DB::table('wilayah_kecamatan')->where('id',$wil->kecamatan)->value('nama'),
					'des' => DB::table('wilayah_desa')->where('id',$wil->desa)->value('nama'),
					'provinsi' => DB::table('wilayah_provinsi')->get(),
					'kabupaten' => DB::table('wilayah_kabupaten')->get(),
					'kecamatan' => DB::table('wilayah_kecamatan')->get(),
					'desa' => DB::table('wilayah_desa')->get()
				);
				return view('admin.user.update',$data);
			}
			return redirect()->back()->with('error', 'Data User Belum Lengkap');
		}
	}

	public function updateTarura(Request $request,$id){
		if (Auth::User()->role == 'admin' || Auth::User()->role == 'super admin') {
			$data = DataTaruna::find($id);
			$data->nama = $request->nama;
			$data->nit = $request->nit;
			// $data->wali = $request->wali;
			$data->tahun = $request->tahun;
			$data->program = $request->program;
			$data->semester = $request->semester;
			$data->kelas = $request->kelas;
			$data->tempatlahir = $request->tempat;
			$data->tgllahir = $request->tgl_lahir;

			if ($data->update()) {
				Session::flash('success', 'Update Data Berhasil');
				return redirect()->back();//->route('dashboard');
			} else {
				Session::flash('errors','Update Data Gagal, Coba Lagi.!');
				return redirect()->back();//->route('user/data');
			}
		}
		abort(404,'Page not found');
	}

	public function updateOrtu(Request $request){
		if (Auth::User()->role == 'admin' || Auth::User()->role == 'super admin') {
			$data = array(
				'nama_ortu' =>$request->ortu,
				'provinsi' =>$request->prov,
				'kabupaten' =>$request->kab,
				'kecamatan' =>$request->kec,
				'desa' =>$request->des,
				'tlp_ortu1' =>$request->notlportu1,
				'tlp_ortu2' =>$request->notlportu2,
				'email_ortu' =>$request->emailortu,
			);
			$update = DB::table('data_orangtuas')->where('id_user',$request->id_user)->update($data);
			if ($update) {
				Session::flash('success', 'Update Data Berhasil');
			return redirect()->back();//->route('dashboard');
		} else {
			Session::flash('errors','Update Data Gagal, Coba Lagi.!');
			return redirect()->back();//->route('user/data');
		}
	}
	abort(404,'Page not found');
}

public function updateMedia(Request $request){
	if (Auth::User()->role == 'admin' || Auth::User()->role == 'super admin') {
		$data = array(
			'tlp_taruna' => $request->notlptaruna,
			'email_taruna' => $request->emailtaruna,
			'facebook' => $request->facebook,
			'instagram' => $request->instagram,
			'twitter' => $request->twitter,
			'ektra_kurikuler' =>$request->ekstrakurikuler,
			'staff' => $request->staff,
		);
		$update = DB::table('data_media_tarunas')->where('id_user',$request->id_user)->update($data);
		if ($update) {
			Session::flash('success', 'Update Data Berhasil');
			return redirect()->back();//->route('dashboard');
		} else {
			Session::flash('errors','Update Data Gagal, Coba Lagi.!');
			return redirect()->back();//->route('user/data');
		}
	}
	abort(404,'Page not found');
}

public function delete($id){
	if (Auth::User()->role == 'super admin') {
		$d1 = DataTaruna::where('id_user',$id);
		$d2 = DataOrangtua::where('id_user',$id);
		$d3 = DataMediaTaruna::where('id_user',$id);

		$d1->delete();
		$d2->delete();
		$d3->delete();

		Session::flash('success', 'Hapus Data Berhasil');
		return redirect()->back();
	}
	abort(404,'Page not found');
}
public function prodi($id){
	$data = DataProdi::where('kode',$id)->first();
	return json_encode($data);
}
public function wali()
{
	$js = User::where('role','operator')->select('id','name','email')->get();
	echo json_encode($js);
}
public function kelas()
{
	$js = User::where('role','operator')->select('kelas1','kelas2')
	->whereNotNull('kelas1')->whereNotNull('kelas2')->get();
	
	$item = array();
	foreach ($js as $key) {
		$item[] = $key->kelas1;
		$item[] = $key->kelas2;
	}
	echo json_encode($item);
}
}
