<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataProdi;
use DataTables;

class prodiController extends Controller
{
	public function index(){
		return view('admin.prodi.index');
	}
	public function insert(Request $request){
		$prod = new DataProdi;
		$prod->kode = $request->kode;
		$prod->prodi = $request->prodi;
		if($prod->save()){
			$status = 200;
			$message = "Tambah Prodi Berhasil";
		}else{
			$status = 400;
			$message = "Gagal Menambahkan Prodi";
		}
		return json_encode([
			"status" => $status,
			"message" => $message,
			'data'  => $prod
		]);
	}
	public function json(){
		$data = DataProdi::all();
		return Datatables::of($data)
		->addIndexColumn()
		->addColumn('action', function($row){

			$btn = '<button class="btn btn-sm btn-rounded btn-danger" onclick="hapus('.$row->id.')">Hapus</button>
			<button class="btn btn-sm btn-rounded btn-warning" onclick="ubah('.$row->id.')">Ubah</button>';

			return $btn;
		})
		->rawColumns(['action'])
		->make(true);
	}

	public function hapus(Request $request){
		$id = $request->id;
		$prod = DataProdi::find($id);
		if($prod->delete()){
			$status = 200;
			$message = "Hapus Data Berhasil";
		}else{
			$status = 400;
			$message = "Gagal Menghapus Data";
		}
		return json_encode([
			"status" => $status,
			"message" => $message,
			'data'  => $prod
		]);
	}
	public function update(Request $request){
		$prod = DataProdi::find($request->id);
		$prod->kode = $request->kode;
		$prod->prodi = $request->prodi;
		if($prod->update()){
			$status = 200;
			$message = "Ubah Prodi Berhasil";
		}else{
			$status = 400;
			$message = "Gagal Mengubah Prodi";
		}
		return json_encode([
			"status" => $status,
			"message" => $message,
			'data'  => $prod
		]);
	}

	public function detProdi($id){
		return json_encode(DataProdi::where('id',$id)->first());
	}

}
