<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\data_pelanggran;
use Session;
use Excel;
use Auth;
use App\Exports\TarunaExport;
use Validator;
use App\User;
use App\modelKelas;

class PelanggaranC extends Controller{
    //
	public function index($tingkat){
		$data = DB::table('tb_pelanggaran')->where('tingkat',$tingkat)->get();
		echo json_encode($data);
	}
	public function jenis_pel($data){
		$jns = DB::table('tb_pelanggaran')->where('id_pelanggaran',$data)->first();
		echo json_encode($jns);
	}

	public function insert(Request $request){
		$validator = Validator::make($request->all(),[
			'semester_pl' => 'required',
			'tgl_pl' => 'required',
		]);
		if($validator->fails()){
			return redirect()->back()->withErrors($validator)->withInput($request->all);
		}
		$data = new data_pelanggran;
		$data->id_user = $request->id_user;
		$data->id_pelanggaran = $request->id_pelanggaran;
		$data->orientasi = $request->orientasi;
		$data->pembentukan = $request->pembentukan;
		$data->pendewasaan = $request->pendewasaan;
		$data->pematangan = $request->pematangan;
		$data->semester = $request->semester_pl;
		$data->waktu = $request->tgl_pl;
		$data->status = 0;
		if ($data->save()) {
			Session::flash('success', 'Insert Data Berhasil');
			return redirect()->back();
		}else{
			Session::flash('error','Insert Data Gagal, Coba Lagi.!');
			return redirect()->back();
		}
	}
	public function show($id){
		$data =	array(
			'data' => DB::table('tb_pelanggaran')
			->select('tb_pelanggaran.tingkat','tb_pelanggaran.jenis_pelanggaran','tb_pelanggaran.tahap_orientasi','tb_pelanggaran.tahap_pembentukan','tb_pelanggaran.tahap_pendewasaan','tb_pelanggaran.tahap_pematangan','data_pelanggrans.id')
			->join('data_pelanggrans','data_pelanggrans.id_pelanggaran','=','tb_pelanggaran.id_pelanggaran')
			->where('data_pelanggrans.id_user',$id)
			->get(),
			'sam' => DB::table('data_pelanggrans')
			->select(
				DB::raw('sum(orientasi) as orientasi'),
				DB::raw('sum(pembentukan) as pembentukan'),
				DB::raw('sum(pendewasaan) as pendewasaan'),
				DB::raw('sum(pematangan) as pematangan'),
			)->where('id_user',$id)->first(),
		);
		dd($data);
		echo json_encode($data);
	}

	public function delete($id){
		if (Auth::User()->role == 'super admin') {
			$del = data_pelanggran::where('id',$id);
			$del->delete();
			Session::flash('success', 'Hapus Data Berhasil');
			return redirect()->back();
		}
		abort(404,'Page not found');
	}

	public function cekpoin(){
		$data = DB::table('view_pelanggaran')->get();
		dd($data);
	}
	public function DownloadExcel($id){
		return Excel::download(new TarunaExport($id), date('d M Y').'.xlsx');
	}
	public function updatePel(Request $request){
		$validator = Validator::make($request->all(),[
			'semester_pl' => 'required',
			'tgl_pl' => 'required',
		]);
		if($validator->fails()){
			return redirect()->back()->withErrors($validator)->withInput($request->all);
		}
		$data = data_pelanggran::find($request->id_updt);
		$data->id_pelanggaran = $request->id_pelanggaran;
		$data->orientasi = $request->orientasi;
		$data->pembentukan = $request->pembentukan;
		$data->pendewasaan = $request->pendewasaan;
		$data->pematangan = $request->pematangan;
		$data->status = 0;
		$data->semester = $request->semester_pl;
		$data->waktu = $request->tgl_pl;
		if ($data->update()) {
			return redirect()->back()->with(['success' => 'Update Data Berhasil']);
		}
	}
	public function getData($id){
		$data = DB::table('tb_pelanggaran')
		->select('tb_pelanggaran.*','data_pelanggrans.id','data_pelanggrans.semester','data_pelanggrans.waktu')
		->join('data_pelanggrans','data_pelanggrans.id_pelanggaran','=','tb_pelanggaran.id_pelanggaran')
		->where('data_pelanggrans.id',$id)
		->first();
		echo json_encode($data);
	}
	public function kelas(){
		$kls = modelKelas::where('id_user',Auth::User()->id)->get();
		echo json_encode($kls);
	}
}
