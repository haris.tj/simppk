<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\modelAbsensi;
use DataTables;

class AbsensiController extends Controller{
	public function insert(Request $request){
		$val = modelAbsensi::where('waktu',date('d M Y'))->count();
		if ($val >= 1) {
			return json_encode([
				'status'	=> 400,
				'message'	=> "User Telah Melakukan Absensi Hari Ini.",
				'total'		=> $val
			]);
		}
		$abs = new modelAbsensi;
		$abs->id_user = $request->id_user;
		$abs->keterangan = $request->keterangan;
		$abs->waktu = date('d M Y');
		if ($abs->save()) {
			return json_encode([
				'status' => 200,
				'message'	=> 'Absensi Berhasil',
				'data' => $abs
			]);
		}
	}
	public function update(Request $request){
		$abs = modelAbsensi::find($request->id);
		$abs->keterangan = $request->keterangan;
		if ($abs->update()) {
			return json_encode([
				'status' => 200,
				'message'	=> 'Ubah Data Absensi Berhasil',
				'data' => $abs
			]);
		}
	}
	public function show($id){
		$data = modelAbsensi::where('id_user',$id);
		return Datatables::of($data)
		->addIndexColumn()
		->addColumn('action', function($row){

			$btn = '<button class="btn btn-sm btn-rounded btn-danger" onclick="hapus_abs('.$row->id.')">Hapus</button>
			<button class="btn btn-sm btn-rounded btn-warning" onclick="ubah_abs('.$row->id.')">Ubah</button>';

			return $btn;
		})
		->rawColumns(['action'])
		->make(true);
	}

	public function detail($id){
		return json_encode(modelAbsensi::where('id',$id)->first());
	}
	public function hapus(Request $request){
		$id = $request->id;
		$prod = modelAbsensi::find($id);
		if($prod->delete()){
			$status = 200;
			$message = "Hapus Data Berhasil";
		}else{
			$status = 400;
			$message = "Gagal Menghapus Data";
		}
		return json_encode([
			"status" => $status,
			"message" => $message,
			'data'  => $prod
		]);
	}
}
