<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use App\User;
use App\UserModel;
use App\DataTaruna;
use App\DataOrangtua;
use App\DataMediaTaruna;


class AuthController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function showFormLogin()
    {
        if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
            //Login Success
        	// return redirect()->route('dashboard');
            return redirect('dashboard');
        }
        return view('login_backup');
    }
    
    public function login(Request $request)
    {
    	$rules = [
    		'email'                 => 'required|email|unique:users,email',
    		'password'              => 'required|string'
    	];
    	
    	$messages = [
    		'email.required'        => 'Email wajib diisi',
    		'email.email'           => 'Email tidak valid',
    		'password.required'     => 'Password wajib diisi',
    		'password.string'       => 'Password harus berupa string'
    	];
    	
    	$validator = Validator::make($request->all(), $rules, $messages);
    	
    	if($validator->fails()){
    		return redirect()->back()->withErrors($validator)->withInput($request->all);
    	}
    	
    	$data = [
    		'email'     => $request->input('email'),
    		'password'  => $request->input('password'),
    	];
    	
    	Auth::attempt($data);
    	
        if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
            //Login Success
            if (Auth::User()->role == 'user') {

                $id = Auth::User()->id;
                $d1 = DataTaruna::where('id_user',$id)->count();
                $d2 = DataTaruna::where('id_user',$id)->count();
                $d3 = DataOrangtua::where('id_user',$id)->count();
                if ($d1 != 1 && $d2 != 1 && $d3 !=1) {
                   return redirect('user/data');
               }
           }
           return redirect()->route('dashboard');

        } else { // false
        	
            //Login Fail
        	Session::flash('error', 'Email atau password salah');
        	return redirect()->route('login');
        }
        
    }
    
    public function showFormRegister()
    {
    	return view('auth.register');
    }
    
    public function register(Request $request)
    {
    	$rules = [
    		'name'                  => 'required|min:3|max:35',
    		'email'                 => 'required|email|unique:users,email',
    		'password'              => 'required|confirmed'
    	];
    	
    	$messages = [
    		'name.required'         => 'Nama Lengkap wajib diisi',
    		'name.min'              => 'Nama lengkap minimal 3 karakter',
    		'name.max'              => 'Nama lengkap maksimal 35 karakter',
    		'email.required'        => 'Email wajib diisi',
    		'email.email'           => 'Email tidak valid',
    		'email.unique'          => 'Email sudah terdaftar',
    		'password.required'     => 'Password wajib diisi',
    		'password.confirmed'    => 'Password tidak sama dengan konfirmasi password'
    	];
    	
    	$validator = Validator::make($request->all(), $rules, $messages);
    	
    	if($validator->fails()){
    		return redirect()->back()->withErrors($validator)->withInput($request->all);
    	}
    	
    	$user = new User;
    	$user->name = ucwords(strtolower($request->name));
    	$user->email = strtolower($request->email);
    	$user->password = Hash::make($request->password);
    	$user->email_verified_at = \Carbon\Carbon::now();
        $user->role = 'user';
        $simpan = $user->save();

        if($simpan){
          Session::flash('success', 'Register berhasil! Silahkan login untuk mengakses data');
          return redirect()->route('login');
      } else {
          Session::flash('errors', ['' => 'Register gagal! Silahkan ulangi beberapa saat lagi']);
          return redirect()->route('register');
      }
  }

  public function logout(){
        Auth::logout(); // menghapus session yang aktif
        return redirect()->route('login');
    }
    public function updatepass(Request $request){
        $usr = User::find($request->id);
        $usr->name = $request->name;
        $usr->email = $request->email;
        $usr->password = Hash::make($request->password);

        if ($usr->update()) {
            return redirect()->back()->with('success','Ubah Data Berhasil');
        }else{
            return redirect()->back()->with('error','Gagal Mengubah Data.');
        }
    }


}