<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class TarunaExport implements WithMultipleSheets
{
	use Exportable;
	protected $id;

	public function __construct($id) {
		$this->id = $id;
	}
	public function sheets(): array
	{
		return [
			0 => new PrestasiExport($this->id),
			1 => new PelanggaranExport($this->id),
			2 => new jasmaniExport($this->id),
			3 => new AbsensiExport($this->id)
		];
	}
}