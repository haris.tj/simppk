<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

use DB;

class PelanggaranExport implements FromCollection,WithTitle,ShouldAutoSize,WithHeadings,WithEvents{
    protected $id;

    public function __construct($id) {
        $this->id = $id;
    }
    public function collection(){
        return DB::table('download_pel_excel')->where('kelas',$this->id)->get();
    }
    public function title(): string{
        return 'Pelanggaran';
    }
    public function headings(): array{
         return [
            'No Induk Taruna',
            'Nama',
            'Kelas',
            'Program',
            'Total Poin',
            'Pelanggaran',
            'Waktu',
        ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; 
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getRowDimension(1)->setRowHeight(20);
                $event->sheet->getColumnDimension('F')->setAutoSize(true);
                    for ($i=0; $i <100 ; $i++) { 
                    $event->sheet->getStyle('F'.$i)->getAlignment()->setWrapText(true);
                    $event->sheet->getStyle('G'.$i)->getAlignment()->setWrapText(true);
                }
            },
        ];
    }
}
