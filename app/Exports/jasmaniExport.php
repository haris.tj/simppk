<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use DB;
use Auth;

class jasmaniExport implements FromCollection,WithTitle,ShouldAutoSize,WithHeadings
{

	protected $id;

	public function __construct($id) {
		$this->id = $id;
	}
	public function collection()
	{
		return DB::table('view_jasmani')->where('kelas',$this->id)->get();
	}
	public function title(): string
	{
		return 'Kebugaran Taruna';
	}
	public function headings(): array
	{
		return [
			'NIT',
			'Nama',
			'Kelas',
			'Push Up',
			'Sit up',
			'Sprint 100m',
			'Lompat Vertikal',
		];
	}
}