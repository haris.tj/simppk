<html>
<head>
  <style>
  @page {
    margin: 0cm 0cm;
  }

  /** Define now the real margins of every page in the PDF **/
  body {
    margin-top: 3cm;
    margin-left: 2cm;
    margin-right: 2cm;
    margin-bottom: 2cm;
  }

  /** Define the header rules **/
  header {
    position: fixed;
    top: 0cm;
    left: 0cm;
    right: 0cm;
    height: 3cm;
  }

  /** Define the footer rules **/
  footer {
    position: fixed; 
    bottom: 0cm; 
    left: 0cm; 
    right: 0cm;
    height: 2cm;
  }
  .alignleft {
    float: left;
  }
  .alignright {
    float: right;
  }
  </style>
</head>
<body>
  <!-- Define header and footer blocks before your content -->
  <header>
    <img src="https://i.postimg.cc/SNwfgZZ0/1.png"/>
  </header>

  <footer>
    <img src="https://i.postimg.cc/DzhdTSkB/2.png"/>
  </footer>

  <!-- Wrap the content of your PDF inside a main tag -->
  <main>
    <br><br><br><br>
    <html>

    <head>
      <meta http-equiv=Content-Type content="text/html; charset=windows-1252">
      <meta name=Generator content="Microsoft Word 14 (filtered)">
      <style>
      <!--
      /* Font Definitions */
      @font-face
      {font-family:Calibri;
        panose-1:2 15 5 2 2 2 4 3 2 4;}
        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal
        {margin-top:0cm;
          margin-right:0cm;
          margin-bottom:10.0pt;
          margin-left:0cm;
          line-height:115%;
          font-size:11.0pt;
          font-family:"Calibri","sans-serif";}
          p.MsoHeader, li.MsoHeader, div.MsoHeader
          {mso-style-link:"Header Char";
          margin:0cm;
          margin-bottom:.0001pt;
          font-size:11.0pt;
          font-family:"Calibri","sans-serif";}
          p.MsoFooter, li.MsoFooter, div.MsoFooter
          {mso-style-link:"Footer Char";
          margin:0cm;
          margin-bottom:.0001pt;
          font-size:11.0pt;
          font-family:"Calibri","sans-serif";}
          p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
          {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:10.0pt;
            margin-left:36.0pt;
            line-height:115%;
            font-size:11.0pt;
            font-family:"Calibri","sans-serif";}
            p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
            {margin-top:0cm;
              margin-right:0cm;
              margin-bottom:0cm;
              margin-left:36.0pt;
              margin-bottom:.0001pt;
              line-height:115%;
              font-size:11.0pt;
              font-family:"Calibri","sans-serif";}
              p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
              {margin-top:0cm;
                margin-right:0cm;
                margin-bottom:0cm;
                margin-left:36.0pt;
                margin-bottom:.0001pt;
                line-height:115%;
                font-size:11.0pt;
                font-family:"Calibri","sans-serif";}
                p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
                {margin-top:0cm;
                  margin-right:0cm;
                  margin-bottom:10.0pt;
                  margin-left:36.0pt;
                  line-height:115%;
                  font-size:11.0pt;
                  font-family:"Calibri","sans-serif";}
                  span.HeaderChar
                  {mso-style-name:"Header Char";
                  mso-style-link:Header;
                  font-family:"Calibri","sans-serif";}
                  span.FooterChar
                  {mso-style-name:"Footer Char";
                  mso-style-link:Footer;
                  font-family:"Calibri","sans-serif";}
                  .MsoChpDefault
                  {font-family:"Calibri","sans-serif";}
                  .MsoPapDefault
                  {margin-bottom:10.0pt;
                    line-height:115%;}
                    /* Page Definitions */
                    @page WordSection1
                    {size:595.3pt 841.9pt;
                      margin:72.0pt 72.0pt 72.0pt 72.0pt;}
                      div.WordSection1
                      {page:WordSection1;}
                      /* List Definitions */
                      ol
                      {margin-bottom:0cm;}
                      ul
                      {margin-bottom:0cm;}
                      -->
                      </style>

                    </head>

                    <body lang=IN>

                      <div class=WordSection1>

                        <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=659
                        style='width:494.45pt;margin-left:-15.9pt;border-collapse:collapse;border:
                        none'>
                        <tr>
                          <td width=84 valign=top style='width:62.8pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                            115%'><span style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Nomor      
                             </span></p>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                            115%'><span style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Klasifikasi
                             </span></p>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                            115%'><span style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Lampiran   
                            </span></p>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                            normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Perihal       
                            </span></p>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                            normal'><span lang=EN-US>&nbsp;</span></p>
                          </td>
                          <td width=349 valign=top style='width:261.85pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                            normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>:</span></p>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                            normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>:
                            Segera</span></p>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                            normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>:
                            -</span></p>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                            normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>:
                            Surat Pemberitahuan</span></p>
                          </td>
                          <td width=226 valign=top style='width:169.8pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>
                            <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
                            margin-left:36.0pt;margin-bottom:.0001pt;line-height:normal'><span
                            style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Surabaya,
                          {{date('d M Y')}}</span></p>
                          <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                          normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
                          <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
                          margin-left:36.0pt;margin-bottom:.0001pt;line-height:normal'><span
                          style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Kepada</span></p>
                          <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                          normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Yth 
                          :   Orang Tua / Wali Taruna</span></p>
                          <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                          normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
                          <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                          normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Mass Haris</span></p>
                          <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                          normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Di</span></p>
                          <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                          normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
                          <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
                          normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>T
                          E M P A T</span></p>
                        </td>
                      </tr>
                    </table>

                    <p class=MsoNormal>&nbsp;</p>

                    <p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-18.0pt'><span
                      style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>1.<span
                      style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;</span></span><span
                      style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Berdasarkan
                      Hasil Berita Acara Pengambilan Keterangan Perwira Batalyon Pusat Pembangunan
                    Karakter Taruna dan Pasis, di dapati bahwa taruna</span></p>

                    <p class=MsoListParagraphCxSpMiddle style='margin-left:72.0pt;text-align:justify'><span
                      style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Nama
                                              : Haris Tanone</span></p>

                    <p class=MsoListParagraphCxSpMiddle style='margin-left:72.0pt;text-align:justify'><span
                      style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Jurusan                        :</span></p>

                      <p class=MsoListParagraphCxSpLast style='margin-left:72.0pt;text-align:justify'><span
                        style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Semester
                      / Program     : 2 / Teknik  Informatika</span></p>

                      <p class=MsoNormal style='margin-left:36.0pt;text-align:justify'><span
                        style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Pada
                        tanggal {{date('d M Y')}} telah melakukan pelanggaran terhadap Pertibtar
                        sesuai dengan pasal ..... ayat .... tingkat .... dengan jenis pelanggaran
                      yaitu Memasuki ruangan taruni tanpa izin.</span></p>

                      <p class=MsoListParagraphCxSpFirst style='text-align:justify;text-indent:-18.0pt'><span
                        style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>2.<span
                        style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;</span></span><span
                        style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Sehubungan
                      dengan hal tersebut diatas, maka bersama dengan ini kami sampaikan :</span></p>

                      <p class=MsoListParagraphCxSpMiddle style='margin-left:72.0pt;text-align:justify;
                      text-indent:-18.0pt'><span style='font-size:12.0pt;line-height:115%;font-family:
                      "Times New Roman","serif"'>a.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
                      </span></span><span style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Surat
                        Peringatan pertama sebagai pemberitahuan untuk orang tua/wali agar dapat ikut
                      memperhatikan dan membina.</span></p>

                      <p class=MsoListParagraphCxSpMiddle style='margin-left:72.0pt;text-align:justify;
                      text-indent:-18.0pt'><span style='font-size:12.0pt;line-height:115%;font-family:
                      "Times New Roman","serif"'>b.<span style='font:7.0pt "Times New Roman"'>&nbsp;
                      </span></span><span style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Surat
                        Peringatan kedua sebagai pemberitahuan dan undangan untuk melakukan pembinaan
                        terhadap taruna terkait  melalui video call dengan Kapus Pembangunan Karakter
                      Poltekpel Surabaya.</span></p>

                      <p class=MsoListParagraphCxSpMiddle style='margin-left:72.0pt;text-align:justify;
                      text-indent:-18.0pt'><span style='font-size:12.0pt;line-height:115%;font-family:
                      "Times New Roman","serif"'>c.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
                      </span></span><span style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Undangan
                        Orang Tua/Wali Untuk hadir ke Pusat Pembangunan Karakter  untuk membahas 
                      terkait pelanggaran yang telah dilakukan  taruna.</span></p>

                      <p class=MsoListParagraphCxSpMiddle style='margin-left:72.0pt;text-align:justify;
                      text-indent:-18.0pt'><span style='font-size:12.0pt;line-height:115%;font-family:
                      "Times New Roman","serif"'>d.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
                      </span></span><span style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Untuk
                        konfirmasi kesediaanya melakukan video call atau  kedatangannya di mohon untuk
                        menghubungi Bapak {{$data->nama}} sebagai Perwira Batalyon di
                      nomor HP {{$data->telepon}}</span></p>

                      <p class=MsoListParagraphCxSpMiddle style='margin-left:72.0pt;text-align:justify'><span
                        style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

                        <p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-indent:-18.0pt'><span
                          style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>3.<span
                          style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;</span></span><span
                          style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Demikian
                        atas perhatian dan kerjasamanya  kami ucapkan terima kasih.</span></p>

                        <p class=MsoListParagraphCxSpMiddle><span style='font-size:12.0pt;line-height:
                          115%;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

                          <p class=MsoListParagraphCxSpMiddle><span style='font-size:12.0pt;line-height:
                            115%;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

                            <p class=MsoListParagraphCxSpMiddle align=right style='text-align:right'><b><span
                              style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>a.n.
                            DIREKTUR POLTEKPEL SURABAYA</span></b></p>

                            <p class=MsoListParagraphCxSpMiddle><b><span
                              style='margin-left:330px; font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>           
                            WAKIL DIREKTUR III</span></b></p>

                            <p class=MsoListParagraphCxSpMiddle align=right style='text-align:right'><span
                              style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

                              <p class=MsoListParagraphCxSpMiddle align=right style='text-align:right'><span
                                style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

                                <p class=MsoListParagraphCxSpMiddle align=right style='text-align:right'><span
                                  style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

                                  <p class=MsoListParagraphCxSpMiddle><b><u><span
                                    style='margin-left:320px; font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>{{$data->nama_direktur}}</span></u></b></p>

                                  <p class=MsoListParagraphCxSpMiddle><span
                                    style='margin-left:380px ;font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>{{$data->penata}}</span></p>

                                  <p class=MsoListParagraphCxSpMiddle><span
                                    style='margin-left:350px; font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>NIP.
                                  {{$data->nip}}</span></p>

                                  <p class=MsoListParagraphCxSpLast><span style='font-size:12.0pt;line-height:
                                    115%;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

                                  </div>

                                </body>

                                </html>

                              </main>
                            </body>
                            </html>
