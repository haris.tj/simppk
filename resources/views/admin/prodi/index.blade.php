@include('new_layout.header')
@include('new_layout.sidebar')
<div class="dashboard-wrapper">
	<div class="container-fluid dashboard-content">
		<!-- ============================================================== -->
		<!-- pageheader -->
		<!-- ============================================================== -->
		<div class="row">
			<!-- ============================================================== -->
			<!-- data table multiselects  -->
			<!-- ============================================================== -->
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

				<div class="card">
					<div class="card-header">
						<div class="row form-group">
							<div class="col-sm-6">
								<button class="btn btn-sm btn-primary btn-rounded" id="tambah">Tambah Data</button>
							</div>
							<div class="col-sm-6 text-right">
								<h2>Data Prodi</h2>
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="tbl_prodi" class="stripe row-border order-column" style="width:100%">
								<thead>
									<tr>
										<th>No</th>
										<th>Kode</th>
										<th>Prodi</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<!-- ============================================================== -->
			<!-- end data table multiselects  -->
			<!-- ============================================================== -->
		</div>
		{{-- MODAL --}}
		<!-- Modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="card border-0">
							<div class="card-body">
								<form id="form-prodi">
									@csrf
									<input type="hidden" name="id" id="id" value="">
									<div class="row form-group">
										<label>Kode</label>
										<input type="number" name="kode" id="kode" required placeholder="Kode Prodi" class="form-control" value="">
									</div>
									<div class="row form-group">
										<label>Prodi</label>
										<input type="text" name="prodi" id="prodi" required placeholder="Nama Prodi" class="form-control" value="">
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-sm btn-rounded btn-primary" id="save">Save</button>
					</div>
				</div>
			</div>
		</div>
		{{-- END MODAL --}}
		<!-- ============================================================== -->
		<!-- end pageheader -->
		<!-- ============================================================== -->

	</div>
	@include('new_layout.footer')
	<script type="text/javascript">
		function cls(){
			$('#exampleModal #id').val('')
			$('#kode').val('')
			$('#prodi').val('')
			$('#exampleModal').modal('hide')
		}
		jQuery(document).ready(function($) {
			$('#tbl_prodi').DataTable({
				processing: true,
				serverSide: true,
				ajax: 'prodi/json',
				columns: [
				{ data: 'id', name: 'id' },
				{ data: 'kode', name: 'kode' },
				{ data: 'prodi', name: 'prodi' },
				{data: 'action', name: 'action', orderable: false, searchable: false},
				]
			});
			$('#tambah').click(function(event) {
				event.preventDefault()
				$('#exampleModal').modal('show')
				cls()
			});
			$('#save').click(function(e) {
				e.preventDefault()
				var url = '';
				var id = $('#exampleModal #id').val()
				var table = $('#tbl_prodi').DataTable();
				if (id == "") {
					url = '{{url('prodi/insert')}}'
				}else{
					url = '{{url('prodi/update')}}'
				}
				$.ajax({
					url: url,
					type: 'POST',
					data: $('#form-prodi').serialize(),
					success: function (res) {
						var rsp = JSON.parse(res);
						if (rsp.status == "200") {
							swal({
								title: "Success",
								text: rsp.message,
								icon: "success",
							});
							table.ajax.reload();
							cls()
						}else{
							swal({
								title: "Error",
								text: rsp.message,
								icon: "error",
							});
							cls()
						}
					}
				});
			});
		});
	</script>
	<script type="text/javascript">
		function hapus(id) {
			var table = $('#tbl_prodi').DataTable();
			swal({
				title: "Apakah Anda Yakin?",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$.ajax({
						url: '{{url('prodi/hapus')}}',
						type: 'POST',
						dataType: 'json',
						data: {
							'id': id,
							'_token': '{{ csrf_token() }}',
						},
						success: function (res) {
							if (res.status == 200) {
								swal({
									title: "Success",
									text: res.message,
									icon: "success",
								});
								table.row($(this)).remove().draw(false);
							}else{
								swal({
									title: "Error",
									text: res.message,
									icon: "error",
								});
							}
						}
					});
				} 
			});			
		}
	</script>
	<script type="text/javascript">
		function ubah(id) {
			jQuery(document).ready(function($) {
				$('#exampleModal').on('shown.bs.modal', function () {
					$('#nama').focus();
				});
				// 
				$.ajax({
					type: "GET",
					url: "/prodi/detail/" + id,
					dataType: "json",
					success: function (response) {
						$('#exampleModal').modal('show')
						$('#exampleModal #id').val(response.id)
						$('#kode').val(response.kode)
						$('#prodi').val(response.prodi)
					}
				});
			});
		}
	</script>