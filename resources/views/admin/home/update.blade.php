<button class="btn-xs btn-rounded btn-warning" data-toggle="modal" data-target="#updateUser{{$dt->id}}">
	<i class="ti-pencil"></i>
</button>
<div class="modal fade" id="updateUser{{$dt->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Update User</h5>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			</div>
			<form action="/user/update" method="post" enctype="multipart/form-data" role="form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id" value="{{$dt->id}}" id="id">
				{{method_field('patch')}}
				<div class="modal-body">
					<div class="row form-group">
						<div class="col-md-12">
							<label for="validationCustom01">Nama</label>
							<input type="text" name="nama" class="form-control" value="{{$dt->name}}">
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12">
							<label class="validationCustom01">Email</label>
							<input type="email" name="email" class="form-control" value="{{$dt->email}}">
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12">
							<label class="validationCustom01">Role</label>
							<select class="form-control" name="role">
								<?php
								$countries = array('super admin','admin','operator','user');
								$current_country = $dt->role;

								foreach($countries as $country) {
									if($country == $current_country) {
										echo '<option selected="selected">'.$country.'</option>';
									} else {
										echo '<option>'.$country.'</option>';
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12">
							<label for="validationCustom01">Password</label>
							<input type="password" name="password" class="form-control" placeholder="Kosongkan Agar tidak mengupdate password.!">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn-sm btn-rounded btn-info">Save</button>
					<button type="button" class="btn-sm btn-rounded btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>