@include('new_layout.header')
@include('new_layout.sidebar')
<div class="dashboard-wrapper">
	<div class="container-fluid dashboard-content">
		<!-- ============================================================== -->
		<!-- pageheader -->
		<!-- ============================================================== -->
		<div class="row">
			<!-- ============================================================== -->
			<!-- data table multiselects  -->
			<!-- ============================================================== -->
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

				<div class="card">
					<div class="card-header">
						@if(session('errors'))
						<div class="alert alert-danger alert-dismissible fade show" role="alert">
							Something it's wrong:
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						@if (Session::has('success'))
						<div class="alert alert-success">
							{{ Session::get('success') }}
						</div>
						@endif
						@if (Session::has('error'))
						<div class="alert alert-danger">
							{{ Session::get('error') }}
						</div>
						@endif
						<div class="row form-group">
							<div class="col-sm-6">
								@include('admin.home.insert')
							</div>
							<div class="col-sm-6 text-right">
								<h2>Data User</h2>
							</div>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="stripe row-border order-column" style="width:100%">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama</th>
										<th>Email</th>
										<th>Role</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1;?>
									@foreach($data as $dt)
									<tr>
										<td class="col-xs-1">{{ $no++ }}</td>
										<td>{{$dt->name}}</td>
										<td>{{$dt->email}}</td>
										<td>{{$dt->role}}</td>
										<td>
											@include('admin.home.update')
											@include('admin.home.delete')
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- ============================================================== -->
			<!-- end data table multiselects  -->
			<!-- ============================================================== -->
		</div>
		<!-- ============================================================== -->
		<!-- end pageheader -->
		<!-- ============================================================== -->

	</div>
	@include('new_layout.footer')
	<script type="text/javascript">
		function detail(id){
			window.location = "/admin/komisioner/detail/"+id;
		}
		function tambah(){
			window.location = "/admin/komisioner/insert";
		}
		function download(){
			window.location = '/admin/kritik-saran/download/excel';
			target = "_blank";
		}
	</script>