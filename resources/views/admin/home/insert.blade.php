<button class="btn-xs btn-rounded btn-primary" data-toggle="modal" data-target="#TambahUser">
	Tambah Data
</button>
<div class="modal fade" id="TambahUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Tambah User</h5>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			</div>
			<form action="/user/insert" method="post" enctype="multipart/form-data" role="form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				{{method_field('patch')}}
				<div class="modal-body">
					<div class="row form-group">
						<div class="col-md-12">
							<label for="validationCustom01">Nama</label>
							<input type="text" name="nama" class="form-control" required>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12">
							<label class="validationCustom01">Email</label>
							<input type="email" name="email" class="form-control" required>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12">
							<label class="validationCustom01">Role</label>
							<select class="form-control" name="role" required id="role">
								<option>Pilih</option>
								<option value="super admin">Super admin</option>
								<option value="admin">Admin</option>
								<option value="operator">Operator</option>
								<option value="user">User</option>
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12">
							<label for="validationCustom01">Password</label>
							<input type="password" name="password" class="form-control" required>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn-sm btn-rounded btn-info">Save</button>
					<button type="button" class="btn-sm btn-rounded btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>