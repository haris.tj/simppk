<button class="btn-xs btn-rounded btn-warning" data-toggle="modal" data-target="#updatePrestasi{{$pr->id}}">
	<i class="ti-pencil"></i>
</button>
{{--  --}}
<div class="modal fade" id="updatePrestasi{{$pr->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Update Prestasi</h5>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			</div>
			<form action="/user/update" method="post" enctype="multipart/form-data" role="form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id" value="{{$pr->id}}" id="id">
				{{method_field('patch')}}
				<div class="modal-body">
					<div class="row form-group">
						<div class="col-md-12">
							<label for="validationCustom01">Bidang</label>
							<select class="form-control" name="bidang_up" id="bidang_up">
								<?php
								$countries = array('akademik','olahraga & seni','taruna','lainnya');
								$current_country = $pr->bidang;

								foreach($countries as $country) {
									if($country == $current_country) {
										echo '<option selected="selected" value="'.$country.'">'.$country.'</option>';
									} else {
										echo '<option value="'.$country.'">'.$country.'</option>';
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12">
							<label class="validationCustom01">Prestasi</label>
							<input type="text" name="prestasi_up" id="prestasi_up" class="form-control" required list="dataPrestasi_up" value="{{$pr->prestasi}}">
							<datalist id="dataPrestasi_up">

							</datalist>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-sm-12">
							<label class="validationCustom01">Poin</label>
							<input type="text" name="poin_up" id="poin_up" class="form-control" value="{{$pr->poin}}" readonly="">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn-sm btn-rounded btn-info">Save</button>
					<button type="button" class="btn-sm btn-rounded btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
	{{--  --}}