@include('new_layout.header')
@include('new_layout.sidebar')
<div class="dashboard-wrapper">
	<div class="dashboard-ecommerce">
		<div class="container-fluid dashboard-content ">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="card">

					<div class="card-header">
						@if ($message = Session::get('errors'))
						<div class="alert alert-danger alert-block">
							<button type="button" class="close" data-dismiss="alert">×</button> 
							<strong>{{ $message }}</strong>
						</div>
						@elseif ($message = Session::get('success'))
						<div class="alert alert-success alert-block">
							<button type="button" class="close" data-dismiss="alert">×</button> 
							<strong>{{ $message }}</strong>
						</div>
						@endif
					</div>
					<div class="card-body">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
							{{-- <div class="section-block"> --}}
								<h5 class="section-title">Outline Tabs</h5>
								<p>Takes the basic nav from above and adds the .nav-tabs class to generate a tabbed interface..</p>
							{{-- </div> --}}
							<div class="tab-outline">
								<ul class="nav nav-tabs" id="myTab2" role="tablist">
									<li class="nav-item">
										<a class="nav-link active" id="tab-outline-one" data-toggle="tab" href="#outline-one" role="tab" aria-controls="home" aria-selected="true">Tab#1</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="tab-outline-two" data-toggle="tab" href="#outline-two" role="tab" aria-controls="profile" aria-selected="false">Tab#2</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="tab-outline-three" data-toggle="tab" href="#outline-three" role="tab" aria-controls="contact" aria-selected="false">Tab#3</a>
									</li>
								</ul>
								<div class="tab-content" id="myTabContent2">
									<div class="tab-pane fade show active" id="outline-one" role="tabpanel" aria-labelledby="tab-outline-one">
										@foreach($data1 as $dt1)
										<form action="/user/data/{{$dt1->id}}/update1" method="post" enctype="multipart/form-data" role="form">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<div class="form-group row">
												<div class="col-md-6">
													<label for="validationCustom01">Nama Lengkap</label>
													<input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap" value="{{$dt1->nama}}" required>
													<div class="valid-feedback">
														Looks good!
													</div>
												</div>
												<div class="col-md-6">
													<label for="validationCustom01">Nomer Induk Taruna</label>
													<input type="number" class="form-control" id="nit" name="nit" placeholder="Nomor Induk Taruna" required value="{{$dt1->nit}}" required>
													<div class="valid-feedback">
														Looks good!
													</div>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-4">
													<label for="validationCustom01">Angkatan / Tahun Masuk</label>
													<input type="number" class="form-control" id="tahun" name="tahun" placeholder="Angkatan / Tahun Angkatan" value="{{$dt1->tahun}}" readonly>
												</div>
												<div class="col-md-4">
													<label for="validationCustom01">Program</label>
													<input type="text" class="form-control" id="program" name="program" placeholder="Program" required value="{{$dt1->program}}" readonly>
													
												</div>
												<div class="col-md-4">
													<label for="validationCustom01">Jurusan</label>
													<select class="form-control" id="semester" name="semester">
														<?php $sms = array(1,2,3,4,5,6,7,8) ?>
														@foreach ($sms as $key)
														@if ($key == $dt1->semester)
														@echo '<option value="{{$dt1->semester}}" selected="selected">{{$dt1->semester}}</option>';
														@else
														@echo '<option value="{{$key}}">{{$key}}</option>';
														@endif
														@endforeach
													</select>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-4">
													<label for="validationCustom01">Kelas</label>
													<input type="text" name="kelas" list="kelas" id="inpKelas" class="form-control" placeholder="Kelas" value="{{$dt1->kelas}}">
													<datalist id="kelas">

													</datalist>
													
												</div>
												<div class="col-md-4">
													<label for="validationCustom01">Tempat Lahir</label>
													<input type="text" class="form-control" id="tempat" name="tempat" placeholder="Tempat Lahir" required value="{{$dt1->tempatlahir}}" required>
													<div class="valid-feedback">
														Looks good!
													</div>
												</div>
												<div class="col-md-4">
													<label for="validationCustom01">Tanggal Lahir</label>
													<input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" placeholder="" required value="{{$dt1->tgllahir}}" required>
													<div class="valid-feedback">
														Looks good!
													</div>
												</div>
											</div>
											@endforeach
											<div class="form-group row">
												<button type="submit" class="btn btn-primary btn-block">Simpan</button>
											</div>
										</form>
									</div>
									<div class="tab-pane fade" id="outline-two" role="tabpanel" aria-labelledby="tab-outline-two">
										@foreach($data2 as $dt2)
										<form action="/user/data/update2" method="post" enctype="multipart/form-data" role="form">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<input type="hidden" name="id_user" value="{{$dt2->id_user}}">
											<div class="form-group row">
												<div class="col-md-12">
													<label for="validationCustom01">Nama Orangtua / Wali</label>
													<input type="text" class="form-control" id="ortu" name="ortu" placeholder="Nama Orangtua / Wali"  value="{{$dt2->nama_ortu}}" required>
													<div class="valid-feedback">Looks good!</div>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-6">
													<label for="validationCustom01">Provinsi</label>
													<select name="prov" class="form-control" id="provinsi">
														<option>Pilih Provinsi</option>
														@foreach($provinsi as $pro)
														@if($pro->nama == $prov)
														@echo '<option value="{{$pro->id}}" selected="selected">{{$pro->nama}}</option>';
														@else
														@echo '<option value="{{$pro->id}}">{{$pro->nama}}</option>';
														@endif
														@endforeach
													</select>
												</div>
												<div class="col-md-6">
													<label for="validationCustom01">Kabupaten</label>
													<select name="kab" class="form-control" id="kabupaten">
														<option value="">Pilih Kabupaten</option>
														@foreach($kabupaten as $kab1)
														@if($kab1->nama == $kab)
														@echo '<option value="{{$kab1->id}}" selected="selected">{{$kab1->nama}}</option>';
														@else
														@echo '<option value="{{$kab1->id}}">{{$kab1->nama}}</option>';
														@endif
														@endforeach
													</select>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-6">
													<label for="validationCustom01">Kecamatan</label>
													<select name="kec" class="form-control" id="kecamatan">
														<option>Select Kecamatan</option>
														@foreach($kecamatan as $kec1)
														@if($kec1->nama == $kec)
														@echo '<option value="{{$kec1->id}}" selected="selected">{{$kec1->nama}}</option>';
														@else
														@echo '<option value="{{$kec1->id}}">{{$kec1->nama}}</option>';
														@endif
														@endforeach
													</select>
												</div>
												<div class="col-md-6">
													<label for="validationCustom01">Desa</label>
													<select name="des" class="form-control" id="desa">
														<option>Select Desa</option>
														@foreach($desa as $des1)
														@if($des1->nama == $des)
														@echo '<option value="{{$des1->id}}" selected="selected">{{$des1->nama}}</option>';
														@else
														@echo '<option value="{{$des1->id}}">{{$des1->nama}}</option>';
														@endif
														@endforeach
													</select>
												</div>
											</div>

											<div class="form-group row">
												<div class="col-md-4">
													<label for="validationCustom01">NoTelepon Orang Tua / Wali 1</label>
													<input type="number" class="form-control" id="notlportu1" name="notlportu1" placeholder="081119999999"value="{{$dt2->tlp_ortu1}}" required>
													<div class="valid-feedback">
														Looks good!
													</div>
												</div>
												<div class="col-md-4">
													<label for="validationCustom01">NoTelepon Orang Tua / Wali 2</label>
													<input type="number" class="form-control" id="notlportu2" name="notlportu2" placeholder="081119999999" value="{{$dt2->tlp_ortu2}}" required>
													<div class="valid-feedback">
														Looks good!
													</div>
												</div>
												<div class="col-md-4">
													<label for="validationCustom01">Email Orang Tua / Wali</label>
													<input type="email" class="form-control" id="emailortu" name="emailortu" placeholder="mail@mail.com" value="{{$dt2->email_ortu}}" required>
													<div class="valid-feedback">
														Looks good!
													</div>
												</div>
											</div>
											@endforeach
											<div class="form-group row">
												<button type="submit" class="btn btn-primary btn-block">Simpan</button>
											</div>
										</form>
									</div>
									<div class="tab-pane fade" id="outline-three" role="tabpanel" aria-labelledby="tab-outline-three">
										@foreach($data3 as $dt3)
										<form action="/user/data/update3" method="post" enctype="multipart/form-data" role="form">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<input type="hidden" name="id_user" value="{{ $dt3->id_user }}">
											<div class="form-group row">
												<div class="col-md-6">
													<label for="validationCustom01">NoTelepon Taruna</label>
													<input type="number" class="form-control" id="notlptaruna" name="notlptaruna" placeholder="081119999999" value="{{$dt3->tlp_taruna}}" required>
													<div class="valid-feedback">Looks good!</div>
												</div>
												<div class="col-md-6">
													<label for="validationCustom01">Email Taruna</label>
													<input type="email" class="form-control" id="emailtaruna" name="emailtaruna" placeholder="mail@mail.com" value="{{$dt3->email_taruna}}" required>
													<div class="valid-feedback">
														Looks good!
													</div>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-4">
													<label for="validationCustom01">Facebook</label>
													<input type="text" class="form-control" id="facebook" name="facebook" placeholder="username" value="{{$dt3->facebook}}" required>
													<div class="valid-feedback">
														Looks good!
													</div>
												</div>
												<div class="col-md-4">
													<label for="validationCustom01">Instagram</label>
													<input type="text" class="form-control" id="instagram" name="instagram" placeholder="username" value="{{$dt3->instagram}}" required>
													<div class="valid-feedback">
														Looks good!
													</div>
												</div>
												<div class="col-md-4">
													<label for="validationCustom01">Twitter</label>
													<input type="text" class="form-control" id="twitter" name="twitter" placeholder="username" value="{{$dt3->twitter}}" required>
													<div class="valid-feedback">
														Looks good!
													</div>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-6">
													<label for="validationCustom01">Ekstra Kurikuler</label>
													<input type="text" name="ekstrakurikuler" id="ekstrakurikuler" class="form-control" value="{{$dt3->ektra_kurikuler}}" readonly>
												</div>
												<div class="col-md-6">
													<label for="validationCustom01">Staff / Tarbi</label>
													<input type="text" class="form-control" id="staff" name="staff" placeholder="Staff / Tarbi"  value="{{$dt3->staff}}" required>
													<div class="valid-feedback">
														Looks good!
													</div>
												</div>
											</div>
											@endforeach
											<div class="form-group row">
												<button type="submit" class="btn btn-primary btn-block">Simpan</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		{{-- </form> --}}
		@include('new_layout.footer')
		<script>
			$(document).ready(function() {
				$("#provinsi").change(function() {
					var url = "{{url('id/kabupaten/')}}/" + $(this).val();
					$('#kabupaten').load(url);
					return false;
				})

				$("#kabupaten").change(function() {
					var url = "{{url('id/kecamatan/')}}/" + $(this).val();
					$('#kecamatan').load(url);
					return false;
				})

				$("#kecamatan").change(function() {
					var url = "{{url('id/desa/')}}/" + $(this).val();
					$('#desa').load(url);
					return false;
				})
			});
		</script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$('#inpKelas').change(function(event) {
					/* Act on the event */
					$( "#inpKelas" ).autocomplete({
						source: "{{url('wali')}}"
					});
				});
			});
		</script>
	</div>
</div>