@include('new_layout.header')
@include('new_layout.sidebar')
<div class="dashboard-wrapper">
	<div class="container-fluid dashboard-content">
		<!-- ============================================================== -->
		<!-- pageheader -->
		<!-- ============================================================== -->
		<div class="row">
			<!-- ============================================================== -->
			<!-- data table multiselects  -->
			<!-- ============================================================== -->
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

				<div class="card">
					<div class="card-header">
						@if ($message = Session::get('error'))
						<div class="alert alert-danger alert-block">
							<button type="button" class="close" data-dismiss="alert">×</button> 
							<strong>{{ $message }}</strong>
						</div>
						@elseif ($message = Session::get('success'))
						<div class="alert alert-success alert-block">
							<button type="button" class="close" data-dismiss="alert">×</button> 
							<strong>{{ $message }}</strong>
						</div>
						@endif
					</div>
					<div class="card-body">
						<div class="form-group row">
							<div class="col-sm-8">
								<h3>Data Taruna</h3>
							</div>
							@if(Auth::user()->role == 'operator')
							<div class="col-sm-2">
								<select class="form-control-sm form-control" name="kls" id="kls">
									{{-- data kelas disini --}}
								</select>
							</div>
							<div class="col-sm-2">
								<button class="btn btn-rounded btn-sm btn-primary" id="cDownload">Download Excel</button>
							</div>
							@endif
						</div>	
						<div class="table-responsive">
							<table id="example" class="stripe row-border order-column" style="width:100%">
								<thead>
									<tr>
										<th>NIT</th>
										<th>Nama</th>
										<th>Angkatan</th>
										<th>Program</th>
										<th>kelas</th>
										<th>Semester</th>
										{{-- <th>Pelanggaran</th> --}}
										<th>Action</th>
									</tr>
								</thead>

								<tbody>
									<?php $no = 1;?>
									@foreach($data as $dt)
									<tr>
										<td>{{$dt->nit}}</td>
										<td>{{$dt->nama}}</td>
										<td>{{$dt->tahun}}</td>
										<td>{{$dt->program}}</td>
										<td>{{$dt->kelas}}</td>
										<td>{{$dt->semester}}</td>
										{{-- <td>0</td> --}}
										<td>
											<button class="btn-xs btn-rounded btn-primary" onclick="ins({{$dt->id_user}})">
												<i class="ti-plus"></i>
											</button>
											<button class="btn-xs btn-rounded btn-success" onclick="detail({{$dt->id_user}})">
												<i class="ti-id-badge"></i>
											</button>

											@if(Auth::User()->role == 'admin' || Auth::User()->role == 'super admin')
											<button class="btn-xs btn-rounded btn-warning" onclick="update({{$dt->id_user}})">
												<i class="ti-pencil"></i>
											</button>
											@endif
											@if(Auth::User()->role == 'super admin')
											@include('admin.user.delete')
											@endif
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- ============================================================== -->
			<!-- end data table multiselects  -->
			<!-- ============================================================== -->
		</div>
		<!-- ============================================================== -->
		<!-- end pageheader -->
		<!-- ============================================================== -->

	</div>
	@include('new_layout.footer')
	<script type="text/javascript">
		function ins(id) {
			window.location = "/taruna/"+id;
		}
		function detail(id){
			window.location = "/user/detail/"+id;
		}
		function update(id){
			window.location = "/user/update/"+id;
		}
	</script>
	<script>
		jQuery(document).ready(function($) {
			var html = '';
			$.ajax({
				url: '{{url('getkelas')}}',
				type: 'GET',
				dataType: 'json',
				success: function(data){
					$.each(data, function(index, val) {
						html += '<option>'+val.nama_kelas+'</option>';
					});
					$('#kls').html(html)
				},
			});
			$('#cDownload').click(function(event) {
				window.location = '{{url('downloadExcel')}}'+'/'+$('#kls').val()
			});
		});
	</script>