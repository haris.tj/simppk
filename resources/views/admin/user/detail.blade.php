@include('new_layout.header')
@include('new_layout.sidebar')
<div class="dashboard-wrapper">
	<div class="dashboard-ecommerce">
		<div class="container-fluid dashboard-content ">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="card">

					<div class="card-header">
						@if ($message = Session::get('error'))
						<div class="alert alert-danger alert-block">
							<button type="button" class="close" data-dismiss="alert">×</button> 
							<strong>{{ $message }}</strong>
						</div>
						@elseif ($message = Session::get('success'))
						<div class="alert alert-success alert-block">
							<button type="button" class="close" data-dismiss="alert">×</button> 
							<strong>{{ $message }}</strong>
						</div>
						@endif
					</div>
					<div class="card-body">
						@if(Auth::User()->role == 'admin' || Auth::User()->role == 'super admin')
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
							<button class="btn-sm btn-warning btn-rounded" name="update" id="update">Update</button>
						</p>
						@endif

						<div class="tab-outline">
							<ul class="nav nav-tabs" id="myTab2" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="tab-outline-one" data-toggle="tab" href="#outline-one" role="tab" aria-controls="home" aria-selected="true">Tab#1</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="tab-outline-two" data-toggle="tab" href="#outline-two" role="tab" aria-controls="profile" aria-selected="false">Tab#2</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="tab-outline-three" data-toggle="tab" href="#outline-three" role="tab" aria-controls="contact" aria-selected="false">Tab#3</a>
								</li>
							</ul>
							<div class="tab-content" id="myTabContent2">
								<div class="tab-pane fade show active" id="outline-one" role="tabpanel" aria-labelledby="tab-outline-one">
									@foreach($data1 as $dt1)
									<input type="hidden" name="id_user" id="id_user" value="{{$dt1->id_user}}">
									<div class="form-group row">
										<div class="col-md-6">
											<label for="validationCustom01">Nama Lengkap</label>
											<input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap" value="{{$dt1->nama}}" readonly>
											<div class="valid-feedback">
												Looks good!
											</div>
										</div>
										<div class="col-md-6">
											<label for="validationCustom01">Nomer Induk Taruna</label>
											<input type="number" class="form-control" id="nit" name="nit" placeholder="Nomor Induk Taruna" required value="{{$dt1->nit}}" readonly>
											<div class="valid-feedback">
												Looks good!
											</div>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-4">
											<label for="validationCustom01">Angkatan / Tahun Masuk</label>
											<input type="number" class="form-control" id="tahun" name="tahun" placeholder="Angkatan / Tahun Angkatan" value="{{$dt1->tahun}}" readonly>
											<div class="valid-feedback">
												Looks good!
											</div>
										</div>
										<div class="col-md-4">
											<label for="validationCustom01">Program</label>
											<input type="text" class="form-control" id="program" name="program" placeholder="Program" required value="{{$dt1->program}}" readonly>
											<div class="valid-feedback">
												Looks good!
											</div>
										</div>
										<div class="col-md-4">
											<label for="validationCustom01">Semester</label>
											<input type="text" class="form-control" id="jurusan" name="jurusan" placeholder="Jurusan" required value="{{$dt1->semester}}" readonly>
											<div class="valid-feedback">Looks good!</div>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-4">
											<label for="validationCustom01">Kelas</label>
											<input type="text" class="form-control" id="kelas" name="kelas" placeholder="Kelas" value="{{$dt1->kelas}}" readonly>
											<div class="valid-feedback">Looks good!</div>
										</div>
										<div class="col-md-4">
											<label for="validationCustom01">Tempat Lahir</label>
											<input type="text" class="form-control" id="tempat" name="tempat" placeholder="Tempat Lahir" required value="{{$dt1->tempatlahir}}" readonly>
											<div class="valid-feedback">
												Looks good!
											</div>
										</div>
										<div class="col-md-4">
											<label for="validationCustom01">Tanggal Lahir</label>
											<input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" placeholder="" required value="{{$dt1->tgllahir}}" readonly>
											<div class="valid-feedback">
												Looks good!
											</div>
										</div>
									</div>
									@endforeach
								</div>
								<div class="tab-pane fade" id="outline-two" role="tabpanel" aria-labelledby="tab-outline-two">
									@foreach($data2 as $dt2)
									<div class="form-group row">
										<div class="col-md-12">
											<label for="validationCustom01">Nama Orangtua / Wali</label>
											<input type="text" class="form-control" id="ortu" name="ortu" placeholder="Nama Orangtua / Wali"  value="{{$dt2->nama_ortu}}" readonly>
											<div class="valid-feedback">Looks good!</div>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-6">
											<label for="validationCustom01">Provinsi</label>
											<input type="text" class="form-control" id="ortu" name="ortu" placeholder="Nama Orangtua / Wali"  value="{{$prov}}" readonly>
										</div>
										<div class="col-md-6">
											<label for="validationCustom01">Kabupaten</label>
											<input type="text" class="form-control" id="ortu" name="ortu" placeholder="Nama Orangtua / Wali"  value="{{$kab}}" readonly>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-6">
											<label for="validationCustom01">Kecamatan</label>
											<input type="text" class="form-control" id="ortu" name="ortu" placeholder="Nama Orangtua / Wali"  value="{{$kec}}" readonly>
										</div>
										<div class="col-md-6">
											<label for="validationCustom01">Desa</label>
											<input type="text" class="form-control" id="ortu" name="ortu" placeholder="Nama Orangtua / Wali"  value="{{$des}}" readonly>
										</div>
									</div>

									<div class="form-group row">
										<div class="col-md-4">
											<label for="validationCustom01">NoTelepon Orang Tua / Wali 1</label>
											<input type="number" class="form-control" id="notlportu1" name="notlportu1" placeholder="081119999999"value="{{$dt2->tlp_ortu1}}" readonly>
											<div class="valid-feedback">
												Looks good!
											</div>
										</div>
										<div class="col-md-4">
											<label for="validationCustom01">NoTelepon Orang Tua / Wali 2</label>
											<input type="number" class="form-control" id="notlportu2" name="notlportu2" placeholder="081119999999" value="{{$dt2->tlp_ortu2}}" readonly>
											<div class="valid-feedback">
												Looks good!
											</div>
										</div>
										<div class="col-md-4">
											<label for="validationCustom01">Email Orang Tua / Wali</label>
											<input type="email" class="form-control" id="emailortu" name="emailortu" placeholder="mail@mail.com" value="{{$dt2->email_ortu}}" readonly>
											<div class="valid-feedback">
												Looks good!
											</div>
										</div>
									</div>
								</div>
								@endforeach
								<div class="tab-pane fade" id="outline-three" role="tabpanel" aria-labelledby="tab-outline-three">
									@foreach($data3 as $dt3)
									<div class="form-group row">
										<div class="col-md-6">
											<label for="validationCustom01">NoTelepon Taruna</label>
											<input type="number" class="form-control" id="notlptaruna" name="notlptaruna" placeholder="081119999999" value="{{$dt3->tlp_taruna}}" readonly>
											<div class="valid-feedback">Looks good!</div>
										</div>
										<div class="col-md-6">
											<label for="validationCustom01">Email Taruna</label>
											<input type="email" class="form-control" id="emailtaruna" name="emailtaruna" placeholder="mail@mail.com" value="{{$dt3->email_taruna}}" readonly>
											<div class="valid-feedback">
												Looks good!
											</div>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-4">
											<label for="validationCustom01">Facebook</label>
											<input type="text" class="form-control" id="facebook" name="facebook" placeholder="username" value="{{$dt3->facebook}}" readonly>
											<div class="valid-feedback">
												Looks good!
											</div>
										</div>
										<div class="col-md-4">
											<label for="validationCustom01">Instagram</label>
											<input type="text" class="form-control" id="instagram" name="instagram" placeholder="username" value="{{$dt3->instagram}}" readonly>
											<div class="valid-feedback">
												Looks good!
											</div>
										</div>
										<div class="col-md-4">
											<label for="validationCustom01">Twitter</label>
											<input type="text" class="form-control" id="twitter" name="twitter" placeholder="username" value="{{$dt3->twitter}}" readonly>
											<div class="valid-feedback">
												Looks good!
											</div>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-md-6">
											<label for="validationCustom01">Ekstra Kurikuler, Posisi</label>
											<input type="text" readonly value="{{$dt3->ektra_kurikuler}}" class="form-control">
										</div>
										<div class="col-md-6">
											<label for="validationCustom01">Staff / Tarbi</label>
											<input type="text" class="form-control" value="{{$dt3->staff}}" readonly>
										</div>
									</div>
									<div class="row form-group">
										<div class="col-sm-12" id="v_ket">
											@if($dt3->staff == "staff")
											<label>Keterangan</label>
											<textarea rows="8" name="keterangan" id="keterangan" readonly class="form-control">
												{{$dt3->keterangan}}
											</textarea>
											@endif
										</div>
									</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	{{-- </form> --}}
	@include('new_layout.footer')
	<script>
		$(document).ready(function() {
			$("#provinsi").change(function() {
				var url = "{{url('id/kabupaten/')}}/" + $(this).val();
				$('#kabupaten').load(url);
				return false;
			})

			$("#kabupaten").change(function() {
				var url = "{{url('id/kecamatan/')}}/" + $(this).val();
				$('#kecamatan').load(url);
				return false;
			})

			$("#kecamatan").change(function() {
				var url = "{{url('id/desa/')}}/" + $(this).val();
				$('#desa').load(url);
				return false;
			})
		});
	</script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('#update').click(function(event) {
				var id = $('#id_user').val();
				window.location = "/user/update/"+id;
			});
		});
	</script>
</div>
</div>