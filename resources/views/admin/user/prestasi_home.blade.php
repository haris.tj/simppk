@include('new_layout.header')
@include('new_layout.sidebar')
<div class="dashboard-wrapper">
	<div class="container-fluid dashboard-content">
		<!-- ============================================================== -->
		<!-- pageheader -->
		<!-- ============================================================== -->
		<div class="row">
			<!-- ============================================================== -->
			<!-- data table multiselects  -->
			<!-- ============================================================== -->
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

				<div class="card">
					<div class="card-header">
						@if(session('errors'))
						<div class="alert alert-danger alert-dismissible fade show" role="alert">
							Something it's wrong:
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						@if (Session::has('success'))
						<div class="alert alert-success">
							{{ Session::get('success') }}
						</div>
						@endif
						@if (Session::has('error'))
						<div class="alert alert-danger">
							{{ Session::get('error') }}
						</div>
						@endif
					</div>
					<div class="card-body">
						{{--  --}}
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
							<div class="form-group">
								<p class="float-left">
									<div class="section-block">
										<h3 class="section-title">{{$user->nit}}</h3>
										<h5 style="color: #808080">{{$user->nama}}</h5>
									</div>
								</p>
								<p class="float-right">
									<a href="{{url('user/data')}}" class="btn btn-danger btn-sm btn-rounded">Kembali</a>
								</p>
							</div>
							<div class="tab-outline">
								<ul class="nav nav-tabs" id="myTab2" role="tablist">
									<li class="nav-item">
										<a class="nav-link active" id="tab-outline-one" data-toggle="tab" href="#outline-one" role="tab" aria-controls="home" aria-selected="true">Prestasi</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="tab-outline-two" data-toggle="tab" href="#outline-two" role="tab" aria-controls="profile" aria-selected="false">Pelanggaran</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="tab-outline-tiga" data-toggle="tab" href="#outline-tiga" role="tab" aria-controls="profile" aria-selected="false">Kebugaran Jamani</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="tab-outline-empat" data-toggle="tab" href="#outline-empat" role="tab" aria-controls="profile" aria-selected="false">Absensi</a>
									</li>
								</ul>
								<div class="tab-content" id="myTabContent2">
									<div class="tab-pane fade show active" id="outline-one" role="tabpanel" aria-labelledby="tab-outline-one">
										<form action="{{url('taruna/prestasi/insert')}}" method="POST" id="form1">
											@csrf
											<input type="hidden" name="id_user" value="{{request()->route('id')}}">
											<input type="hidden" name="id_prests" id="id_prests" value="">
											<input type="hidden" name="id_pr" id="id_pr" value="">
											<input type="hidden" id="act" value="">
											<div class="form-group row">
												<div class="col-sm-4">
													<label>Bidang Prestasi</label>
													<select class="form-control" name="bidang" id="bidang" required>
														<option value="">Pilih</option>
														<option value="akademik">Akademik</option>
														<option value="olahraga & seni">Olahraga & Seni</option>
														<option value="taruna">Taruna</option>
														<option value="non akademik">Non Akademik</option>
														<option value="lainnya">Lainnya</option>
													</select>
												</div>
												<div class="col-sm-7">
													<label>Prestasi</label>
													<input type="text" name="prestasi" id="prestasi" class="form-control" required list="dataPrestasi">
													<datalist id="dataPrestasi">

													</datalist>
												</div>
												<div class="col-sm-1">
													<label>Poin</label>
													<input type="poin" name="poin" id="poin" readonly="" placeholder="0" class="form-control">
												</div>
											</div>
											<div class="form-group row">
												<div class="col-sm-4">
													<label>Semester</label>
													<select name="semester" id="semester" class="form-control" required>
														<option>Pilih</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
													</select>
												</div>
												<div class="col-sm-4">
													<label>Tanggal</label>
													<input type="date" name="tgl" id="tgl" class="form-control" required>
												</div>
											</div>
											<div class="form-group">
												<button type="submit" class="btn btn-secondary btn-sm btn-rounded">Simpan</button>
											</div>
										</form>
										<hr>
										{{-- <a href="#" class="btn btn-secondary">Go somewhere</a> --}}
										<div class="table-responsive">
											<table id="pelTable" class="table table-hover" style="width:100%">
												<thead>
													<tr>
														<th>Bidang</th>
														<th>Prestasi</th>
														<th>Poin</th>
														@if (Auth::User()->role == 'super admin' || Auth::User()->role == 'admin')
														<th>Action</th>
														@endif
													</tr>
												</thead>
												<tbody>
													@foreach($prestasi as $pr)
													<tr>
														<td>{{$pr->bidang}}</td>
														<td>{{$pr->prestasi}}</td>
														<td>{{$pr->poin}}</td>
														@if(Auth::User()->role == 'admin' || Auth::User()->role =='super admin')
														<td>
															@if(Auth::User()->role == 'super admin')
															@include('admin.user.delete_prestasi')
															@endif
															<button class="btn-xs btn-rounded btn-warning" onclick="update({{$pr->id}})">
																<i class="ti-pencil"></i>
															</button>
														</td>
														@endif
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
									<div class="tab-pane fade" id="outline-two" role="tabpanel" aria-labelledby="tab-outline-two">
										{{-- TAP 2 --}}
										<form action="{{url('user/pelanggaran/insert')}}" method="post" enctype="multipart/form-data" role="form" id="form2">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<input type="hidden" name="id_user" id="id_user" value="{{request()->route('id')}}">
											<input type="hidden" name="id_pelanggaran" id="id_pelanggaran" value="">
											<input type="hidden" name="id_updt" id="id_updt" value="">
											<div class="modal-body">
												<div class="row form-group">
													<div class="col-md-3">
														<label for="validationCustom01">Tingkat Pelanggaran</label>
														<select class="form-control" name="tingkat" id="tingkat" required>
															<option>Pilih</option>
															<option value="utama">Utama</option>
															<option value="pertama">Pertama</option>
															<option value="kedua">Kedua</option>
															<option value="ketiga">Ketiga</option>
															<option value="keempat">Keempat</option>
														</select>
													</div>
													<div class="col-sm-9">
														<label class="validationCustom01">Jenis Pelanggaran</label>
														<input type="text" name="jenis" id="jenis" class="form-control" required list="data">
														<datalist id="data">
														</datalist>
													</div>
												</div>
												<div class="row form-group">
													<div class="col-sm-3">
														<label class="validationCustom01">Orientasi</label>
														<input type="text" name="orientasi" id="orientasi" class="form-control" value="" readonly>
													</div>
													<div class="col-sm-3">
														<label class="validationCustom01">Pembentukan</label>
														<input type="text" name="pembentukan" id="pembentukan" class="form-control" value="" readonly>
													</div>
													<div class="col-sm-3">
														<label class="validationCustom01">Pendewasaan</label>
														<input type="text" name="pendewasaan" id="pendewasaan" class="form-control" value="" readonly>
													</div>
													<div class="col-sm-3">
														<label class="validationCustom01">Pematangan</label>
														<input type="text" name="pematangan" id="pematangan" class="form-control" value="" readonly>
													</div>
												</div>
												<div class="row form-group">
													<div class="col-sm-12">
														<label for="validationCustom01">Pembinaan Khusus</label>
														<input type="text" name="pembinaan_khusus" id="pembinaan_khusus" class="form-control" readonly>
													</div>
												</div>
												<div class="row form-group">
													<div class="col-sm-2">
														<label for="validationCustom01">Masa Pembinaan</label>
														<input type="text" name="masa_pembinaan" id="masa_pembinaan" class="form-control" readonly>
													</div>
													<div class="col-sm-5">
														<label for="validationCustom01">Catatan</label>
														<input type="text" name="catatan" id="catatan" class="form-control" readonly>
													</div>
													<div class="col-sm-2">
														<label>Semester</label>
														<select name="semester_pl" id="semester_pl" class="form-control" required>
															<option>Pilih</option>
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
															<option value="5">5</option>
															<option value="6">6</option>
															<option value="7">7</option>
															<option value="8">8</option>
														</select>
													</div>
													<div class="col-sm-3">
														<label>Tanggal</label>
														<input type="date" name="tgl_pl" id="tgl_pl" class="form-control">
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="submit" class="btn-sm btn-rounded btn-info" id="save_pelanggaran" value="ins">Save</button>
											</div>
										</form>
										<hr>
										<h5>Total Poin</h5>
										<div class="form-group row">
											<div class="col-sm-3">
												<label>Orientasi</label>
												<input type="text" name="t_orientasi" value="{{$sam->orientasi}}" readonly="" class="form-control">
											</div>
											<div class="col-sm-3">
												<label>Pembentukan</label>
												<input type="text" name="t_orientasi" value="{{$sam->pembentukan}}" readonly="" class="form-control">
											</div>
											<div class="col-sm-3">
												<label>Pendewasaan</label>
												<input type="text" name="t_orientasi" value="{{$sam->pendewasaan}}" readonly="" class="form-control">
											</div>
											<div class="col-sm-3">
												<label>Pematangan</label>
												<input type="text" name="t_orientasi" value="{{$sam->pematangan}}" readonly="" class="form-control">
											</div>
										</div>
										<div class="table-responsive">
											<table id="tb_pelanggaran" class="table table-hover" style="width:100%">
												<thead>
													<tr>
														<th>Tingkat</th>
														<th>Jenis Pelanggaran</th>
														<th>Orientasi</th>
														<th>Pembentukan</th>
														<th>Pendewasaan</th>
														<th>Pematangan</th>
														@if (Auth::User()->role == 'super admin' || Auth::User()->role == 'admin')
														<th>Action</th>
														@endif
													</tr>
												</thead>
												<tbody id="data-user-pelangrn">
													@foreach($pelanggaran as $pl)
													<tr>
														<td>{{$pl->tingkat}}</td>
														<td>{{$pl->jenis_pelanggaran}}</td>
														<td>{{$pl->tahap_orientasi}}</td>
														<td>{{$pl->tahap_pembentukan}}</td>
														<td>{{$pl->tahap_pendewasaan}}</td>
														<td>{{$pl->tahap_pematangan}}</td>
														@if(Auth::User()->role == 'admin' || Auth::User()->role =='super admin')
														<td>
															@if(Auth::User()->role == 'super admin')
															@include('admin.user.delete_pelanggaran')
															@endif
															<button class="btn-xs btn-rounded btn-warning" onclick="updatePl({{$pl->id}})">
																<i class="ti-pencil"></i>
															</button>
														</td>
														@endif
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
										{{--END TAP 2  --}}
									</div>
									{{-- tab 3 --}}
									<div class="tab-pane fade" id="outline-tiga" role="tabpanel" aria-labelledbu="tab-outline-tiga">
										<div class="row">
											<button class="btn btn-primary btn-sm btn-rounded" id="tambah-jasmani">Tambah</button>
										</div>
										<div class="table-responsive py-3">
											<table class="stripe row-border order-column" style="width:100%" id="tbl_bodymass">
												<thead>
													<tr>
														<th>No</th>
														<th>Push Up</th>
														<th>Sit UP</th>
														<th>Sprint 100m</th>
														<th>Lompat Vertikal</th>
														@if (Auth::User()->role == 'super admin' || Auth::User()->role == 'admin')
														<th>Aksi</th>
														@endif
													</tr>
												</thead>
												<tbody>
													
												</tbody>
											</table>
										</div>
										{{-- MODAL --}}
										<!-- Modal -->
										<div class="modal fade" id="jasmani-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">&times;</span>
														</button>
													</div>
													<div class="modal-body">
														<form id="jasmani-form">
															@csrf
															<input type="hidden" name="id" id="id" value="">
															<input type="hidden" name="id_user" id="id_user" value="{{request()->route('id')}}">
															<div class="form-group">
																<label>Jumlah Push Up</label>
																<input type="number" name="pushup" id="pushup" class="form-control" required>
															</div>
															<div class="form-group">
																<label>Jumlah Sit Up</label>
																<input type="number" name="situp" id="situp" class="form-control" required>
															</div>
															<div class="form-group">
																<label>Waktu Sprint 100 m</label>
																<input type="text" name="lari" id="lari" class="form-control" required>
															</div>
															<div class="form-group">
																<label>Jangkauan Lompat Vertikal</label>
																<input type="text" name="lompat" id="lompat" class="form-control" required>
															</div>
														</form>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-secondary btn-sm btn-rounded" id="save-jasmani">Save</button>
													</div>
												</div>
											</div>
										</div>
										{{-- END MODAL --}}
									</div>
									{{-- tab 4 --}}
									<div class="tab-pane fade" id="outline-empat" role="tabpanel" aria-labelledbu="tab-outline-empat">
										@if(Auth::User()->role == 'operator')
										<div class="row">
											<button class="btn btn-primary btn-sm btn-rounded" id="tambah-absensi">Tambah</button>
										</div>
										@endif
										<div class="table-responsive py-3">
											<table class="stripe row-border order-column" style="width:100%" id="tbl_absensi">
												<thead>
													<tr>
														<th>No</th>
														<th>Keterangan</th>
														<th>Waktu</th>
														@if(Auth::User()->role == 'operator')
														<th>Aksi</th>
														@endif
													</tr>
												</thead>
												<tbody>
													
												</tbody>
											</table>
										</div>
										{{-- MODAL --}}
										<!-- Modal -->
										<div class="modal fade" id="absensi-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">&times;</span>
														</button>
													</div>
													<form id="absensi-form">
														@csrf
														<div class="modal-body">
															<input type="hidden" name="id" id="id" value="">
															<input type="hidden" name="id_user" id="id_user" value="{{request()->route('id')}}">
															<div class="form-group">
																<label>Keterangan</label>
																<select name="keterangan" id="keterangan" class="form-control" required>
																	<option value="masuk">Masuk</option>
																	<option value="sakit">Sakit</option>
																	<option value="ijin">Ijin</option>
																	<option value="tanpa keterangan">Tanpa Keterangan</option>
																	<option value="cuti akademik">Cuti Akademik</option>
																</select>
															</div>
														</div>
														<div class="modal-footer">
															<button type="submit" class="btn btn-secondary btn-sm btn-rounded" id="save-absensi">Save</button>
														</div>
													</form>
												</div>
											</div>
										</div>
										{{-- END MODAL --}}
									</div>
								</div>
							</div>
						</div>
						{{--  --}}
					</div>
				</div>
			</div>
			<!-- ============================================================== -->
			<!-- end data table multiselects  -->
			<!-- ============================================================== -->
		</div>
		<!-- ============================================================== -->
		<!-- end pageheader -->
		<!-- ============================================================== -->

	</div>

	@include('new_layout.footer')
	{{-- script 1 unntuk poin prestassi --}}
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			// function clear berfungsi untuk membersihkan
			function clear(){
				$('#prestasi').val('')
				$('#poin').val('')
			}
			// get data prestasi berdasarkan dropdown yang dipilih
			$('#bidang').change(function(event) {
				clear()
				var bidang = $('#bidang').val();
				$.ajax({
					url: '{{url('taruna/prestasi')}}'+'/'+bidang,
					type: 'GET',
					dataType: 'json',
					success: function(data){
						var html = '';
						$.each(data, function(index, val) {
							html += '<option value="'+val.prestasi+'" label="'+val.id_pres+'">';
						});
						$('#dataPrestasi').html(html)
					}
				});
			});
			// menampilkan poin berdasarkan jenis prestasi yang dipilih
			$('#prestasi').change(function(event) {
				var id = $("#dataPrestasi option[value='" + $("#prestasi").val() + "']").attr("label");
				$.ajax({
					url: '{{url('taruna/prestasi/det')}}'+'/'+id,
					type: 'GET',
					dataType: 'json',
					success: function(data){
						$('#poin').val(data.poin)
						$('#id_prests').val(data.id_pres)
					}
				});
			});
		});
		// function untuk button edit
		function update(id){
			jQuery(document).ready(function($) {
				$('#form1').attr('action', '{{url('taruna/prestasi/update')}}');
				$.ajax({
					url: '{{url('taruna/prestasi/get')}}'+'/'+id,
					type: 'GET',
					dataType: 'json',
					success: function(data){
						$('#bidang').val(data.bidang)
						$('#prestasi').val(data.prestasi)
						$('#poin').val(data.poin)
						$('#id_pr').val(data.id)
						$('#semester').val(data.semester)
						$('#tgl').val(data.waktu)
						$('#id_prests').val(data.id_prestasi)
					}
				});
			});
		}
	</script>
	{{-- scrip 2 untuk poin kesalahan --}}
	<script type="text/javascript">
		function cls(){
			$('#orientasi').val('');
			$('#pembentukan').val('');
			$('#pendewasaan').val('');
			$('#pematangan').val('');
			$('#pembinaan_khusus').val('');
			$('#masa_pembinaan').val('');
			$('#catatan').val('');
			$('#jenis').val('');
		}
		jQuery(document).ready(function($) {
			// menampilkan jenis pelanggaran ketika tinggkat pelanggaran diganti.
			$('#tingkat').change(function(event) {
				cls()
				var tingkat = $('#tingkat').val()
				$.ajax({
					url: '{{url('user/pelanggaran')}}'+'/'+tingkat,
					type: 'GET',
					dataType: 'json',
					success: function(response) {
						var html = '';
						$.each(response,function(index, el) {	
							// console.log(el)
							html += '<option value="'+el.jenis_pelanggaran+'" label="'+el.id_pelanggaran+'">';
						});
						$('#data').html(html);
					}
				});	
			});
			//menampilkan detail dari poin kesalahan yang dipilih
			$('#jenis').change(function(event) {

				var id = $("#data option[value='" + $("#jenis").val() + "']").attr("label");
				$('#id_pelanggaran').val(id);

				$.ajax({
					url: '{{url('user/pelanggaran/jenis')}}'+'/'+id,
					type: 'GET',
					dataType: 'json',
					success: function(res){
							// console.log(res);
							$('#orientasi').val(res.tahap_orientasi);
							$('#pembentukan').val(res.tahap_pembentukan);
							$('#pendewasaan').val(res.tahap_pendewasaan);
							$('#pematangan').val(res.tahap_pematangan);
							$('#pembinaan_khusus').val(res.pembinaan_khusus);
							$('#masa_pembinaan').val(res.masa_pembinaan_khusus);
							$('#catatan').val(res.catatan);
							$('#tingkat').val(res.tingkat);
						}
					});
			});
		});
		function updatePl(id){
			jQuery(document).ready(function($) {
				$('#form2').attr('action', '{{url('taruna/pelanggaran/update')}}');
				$.ajax({
					url: '{{url('taruna/pelanggaran/get')}}'+'/'+id,
					type: 'GET',
					dataType: 'json',
					success: function(data){
						$('#id_updt').val(data.id)
						$('#orientasi').val(data.tahap_orientasi);
						$('#pembentukan').val(data.tahap_pembentukan);
						$('#pendewasaan').val(data.tahap_pendewasaan);
						$('#pematangan').val(data.tahap_pematangan);
						$('#pembinaan_khusus').val(data.pembinaan_khusus);
						$('#masa_pembinaan').val(data.masa_pembinaan_khusus);
						$('#catatan').val(data.catatan);
						$('#tingkat').val(data.tingkat);
						$('#jenis').val(data.jenis_pelanggaran)
						$('#semester_pl').val(data.semester)
						$('#tgl_pl').val(data.waktu)
						$('#id_pelanggaran').val(data.id_pelanggaran)
					}
				});
			});
		}
	</script>
	<script type="text/javascript">
		$(document).ready(function() {

			function clss() {
				$('#pushup').val('')
				$('#id').val('')
				$('#situp').val('')
				$('#lari').val('')
				$('#lompat').val('')
				$('#jasmani-modal').modal('hide')
			}
			$('#tbl_bodymass').DataTable({
				processing: true,
				serverSide: true,
				ajax: '/taruna/jasmaniJson/'+'{{request()->route('id')}}',
				columns: [
				{ data: 'id', name: 'id' },
				{ data: 'pusup', name: 'pusup' },
				{ data: 'situp', name: 'situp' },
				{ data: 'lari', name: 'lompat' },
				{ data: 'lompat', name: 'lari' },
				@if(Auth::User()->role == 'admin' || Auth::User()->role == 'super admin')
				{data: 'action', name: 'action', orderable: false, searchable: false},
				@endif
				]
			});
			$('#tambah-jasmani').click(function(event) {
				$('#jasmani-modal').modal('show')
				$('#jasmani-modal #id').val('')
				clss()
			});
			$('#save-jasmani').click(function(event) {
				event.preventDefault()
				var table = $('#tbl_bodymass').DataTable();
				var url = ''

				if ($('#jasmani-modal #id').val() == "") {
					url = '/taruna/jasmani/insert' 
				}else{
					url = '/taruna/jasmani/update'
				}
				
				$.ajax({
					url: url,
					type: 'POST',
					dataType: 'json',
					data: $('#jasmani-form').serialize(),
					success: function (res) {
						if (res.status == "200") {
							swal({
								title: "Success",
								text: res.message,
								icon: "success",
							});
							table.ajax.reload();
							clss()
						}else{
							swal({
								title: "Error",
								text: res.message,
								icon: "error",
							});
							clss()
						}
					}
				});
			});
		});
		// hapus jasmani
		function hapus(id) {
			swal({
				title: "Apakah Anda Yakin?",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			}).then((willDelete) => {
				if (willDelete) {
					$.post('/taruna/jasmani/hapus', {_token: '{{ csrf_token() }}', id: id}, function(data, textStatus, xhr) {
						var table = $('#tbl_bodymass').DataTable()
						var res = JSON.parse(data)
						if (res.status == "200") {
							swal({
								title: "Success",
								text: res.message,
								icon: "success",
							});
							table.row($(this)).remove().draw(false);
						}else{
							swal({
								title: "Error",
								text: res.message,
								icon: "error",
							});
						}
					});
				}
			})
		}
		function ubah(id) {
			// $('#jasmani-modal').modal('show')
			$.get('/taruna/getjasmani/'+id, function(data) {
				var res = JSON.parse(data)
				$('#jasmani-modal').modal('show')
				$('#jasmani-modal #id').val(res.id)
				$('#pushup').val(res.pusup)
				$('#situp').val(res.situp)
				$('#lari').val(res.lari)
				$('#lompat').val(res.lompat)
			});
		}
	</script>
	{{-- script absensi --}}
	<script type="text/javascript">
		$(document).ready(function() {
			function clears(){
				$('#keterangan').val('Pilih')
				$('#absensi-modal #id').val('')
				$('#absensi-modal').modal('hide')
			}
			$('#tbl_absensi').DataTable({
				processing: true,
				serverSide: true,
				ajax: '/absensi/json/'+'{{request()->route('id')}}',
				columns: [
				{ data: 'id', name: 'id' },
				{ data: 'keterangan', name: 'keterangan' },
				{ data: 'waktu', name: 'waktu' },
				@if(Auth::User()->role == 'operator')
				{data: 'action', name: 'action', orderable: false, searchable: false},
				@endif
				]
			});
			$('#tambah-absensi').click(function(event) {
				$('#absensi-modal').modal('show')	
				clears()	
			});
			$('#absensi-form').submit(function(event) {
				event.preventDefault()
				var table = $('#tbl_absensi').DataTable();
				var url = '';
				var id = $('#absensi-modal #id').val()
				if (id == "") {
					url = '{{url('absensi/insert')}}'
				}else{
					url = '{{url('absensi/update')}}'
				}
				$.ajax({
					url: url,
					type: 'POST',
					data: $('#absensi-form').serialize(),
					success: function(data){
						var res = JSON.parse(data)
						if (res.status == 200) {
							swal({
								title: "Success",
								text: res.message,
								icon: "success",
							});
							table.ajax.reload();
							clears()
						}else{
							swal({
								title: "Error",
								text: res.message,
								icon: "error",
							});
							clears()
						}
					}
				});
			});
		});
	</script>
	<script type="text/javascript">
		function ubah_abs(id){
			$.get('/absensi/detail/'+id, function(data) {
				var res = JSON.parse(data)
				$('#absensi-modal #id').val(id)
				$('#absensi-modal #keterangan').val(res.keterangan)
				$('#absensi-modal').modal('show')
			});
		}
		function hapus_abs(id) {
			var table = $('#tbl_absensi').DataTable();
			swal({
				title: "Apakah Anda Yakin?",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then((willDelete) => {
				if (willDelete) {
					$.ajax({
						url: '{{url('absensi/hapus')}}',
						type: 'POST',
						dataType: 'json',
						data: {
							'id': id,
							'_token': '{{ csrf_token() }}',
						},
						success: function (res) {
							if (res.status == 200) {
								swal({
									title: "Success",
									text: res.message,
									icon: "success",
								});
								table.row($(this)).remove().draw(false);
							}else{
								swal({
									title: "Error",
									text: res.message,
									icon: "error",
								});
							}
						}
					});
				} 
			});	
		}
	</script>
