@include('new_layout.header')
@include('new_layout.sidebar')
<div class="dashboard-wrapper">
  <div class="container-fluid dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
      <!-- ============================================================== -->
      <!-- data table multiselects  -->
      <!-- ============================================================== -->
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

        <div class="card">
          <div class="card-header">
            @if(session('errors'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              Something it's wrong:
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif
            @if (Session::has('success'))
            <div class="alert alert-success">
              {{ Session::get('success') }}
            </div>
            @endif
            @if (Session::has('error'))
            <div class="alert alert-danger">
              {{ Session::get('error') }}
            </div>
            @endif
            <div class="form-group row">
              <div class="col-sm-6">
                <div class="float-left">
                  <button class="btn btn-sm btn-primary btn-rounded" id="tambah-pres">Tambah Prestasi</button>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="float-right">
                  <h3>Master Data Prestasi</h3>
                </div>
              </div>
            </div>
          </div>
          <div  class="card-body">
            {{-- tambah form muncul disini. --}}
            <form id="form-prestasi" method="POST" action="#">

            </form>
            {{-- end dari tambah from --}}
            <div class="table-responsive">
              <br>
              <table id="ms_prestasi"  class="stripe row-border order-column" style="width:100%">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Bidang</th>
                    <th>Prestasi</th>
                    <th>Poin</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1?>
                  @foreach($data as $dt)
                  <tr>
                    <td>{{$no++}}</td>
                    <td>{{$dt->bidang}}</td>
                    <td>{{\Illuminate\Support\Str::lower($dt->prestasi)}}</td>
                    <td>{{$dt->poin}}</td>
                    <td>
                      @include('admin.prestasi.delete')
                      <button class="btn-xs btn-rounded btn-warning" onclick="update({{$dt->id_pres}})">
                        <i class="ti-pencil"></i>
                      </button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- ============================================================== -->
        <!-- end data table multiselects  -->
        <!-- ============================================================== -->
      </div>
      <!-- ============================================================== -->
      <!-- end pageheader -->
      <!-- ============================================================== -->

    </div>
    @include('new_layout.footer')
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        $('#tambah-pres').click(function(event) {
          $.ajax({
            url: '{{url('prestasi/html')}}',
            type: 'GET',   
            success: function(data){
              $('#form-prestasi').attr('action', '{{url('prestasi/insert')}}');
              $('#form-prestasi').html(data.html)
            }
          });
        });
      });
      function update(id) {
        jQuery(document).ready(function($) {
          $.ajax({
            url: '{{url('prestasi/html')}}',
            type: 'GET',   
            success: function(data){
              $('#form-prestasi').attr('action', '{{url('prestasi/update')}}');
              $('#form-prestasi').html(data.html)
            }
          });
          $.ajax({
            url: '{{url('prestasi/view/')}}'+'/'+id,
            type: 'GET',
            dataType: 'json',
            success: function(data){
              $('#id_prs').val(data.id_pres)
              $('#bidang').val(data.bidang)
              $('#prestasi').val(data.prestasi)
              $('#poin').val(data.poin)
            }
          });
        }); 
      }
    </script>