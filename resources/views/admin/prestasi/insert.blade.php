@csrf
<div class="form-group row">
	<input type="hidden" name="id_prs" id="id_prs" value="">
	<div class="col-sm-3">
		<label>Bidang</label>
		<select class="form-control-sm form-control" name="bidang" id="bidang" required>
			<option value="">Pilih</option>
			<option value="akademik">Akademi</option>
			<option value="non akademik">Non Akademi</option>
			<option value="olahraga & seni">Olahraga & Seni</option>
			<option value="taruna">Taruna</option>
			<option value="lainnya">Lainnya</option>
		</select>
	</div>
	<div class="col-sm-8">
		<label>Prestasi</label>
		<input type="text" name="prestasi" id="prestasi" placeholder="Masukan Prestasi" class="form-control-sm form-control" required>
	</div>
	<div class="col-sm-1">
		<label>Poin</label>
		<input type="number" name="poin" id="poin" placeholder="0" class="form-control-sm form-control" required>
	</div>
</div>
<div class="float-right">
	<button class="btn btn-sm btn-rounded btn-secondary">Simpan</button>
</div>