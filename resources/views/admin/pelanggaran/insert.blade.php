<button class="btn-xs btn-rounded btn-primary" data-toggle="modal" 
data-target="#plgrnUser" onclick="pelanggaran({{$dt->id_user}})">
<i class="ti-plus"></i>
</button>
<div class="modal fade" id="plgrnUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Pelanggaran</h5>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
			</div>
			<div class="modal-body">
				{{-- <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 mb-5"> --}}
					<div class="tab-outline">
						<ul class="nav nav-tabs" id="myTab2" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="tab-outline-one" data-toggle="tab" href="#outline-one" role="tab" aria-controls="home" aria-selected="true">Insert</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="tab-outline-two" data-toggle="tab" href="#outline-two" role="tab" aria-controls="profile" aria-selected="false">Detail</a>
							</li>
						</ul>
						<div class="tab-content" id="myTabContent2">
							<div class="tab-pane fade show active" id="outline-one" role="tabpanel" aria-labelledby="tab-outline-one">
								<form action="{{url('user/pelanggaran/insert')}}" method="post" enctype="multipart/form-data" role="form">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									{{method_field('patch')}}
									<input type="hidden" name="id_user" id="id_user" value="">
									<input type="hidden" name="idplg" id="idplg" value="">
									<input type="hidden" name="id_pelanggaran" id="id_pelanggaran" value="">
									<div class="modal-body">
										<div class="row form-group">
											<div class="col-md-12">
												<label for="validationCustom01">Tingkat Pelanggaran</label>
												<select class="form-control" name="tingkat" id="tingkat" required>
													<option>Pilih</option>
													<option value="utama">Utama</option>
													<option value="pertama">Pertama</option>
													<option value="kedua">Kedua</option>
													<option value="ketiga">Ketiga</option>
													<option value="keempat">Keempat</option>
												</select>
											</div>
										</div>
										<div class="row form-group">
											<div class="col-sm-12">
												<label class="validationCustom01">Jenis Pelanggaran</label>
												<input type="text" name="jenis" id="jenis" class="form-control" required list="data">
												<datalist id="data">
												</datalist>
											</div>
										</div>
										<div class="row form-group">
											<div class="col-sm-3">
												<label class="validationCustom01">Orientasi</label>
												<input type="text" name="orientasi" id="orientasi" class="form-control" value="">
											</div>
											<div class="col-sm-3">
												<label class="validationCustom01">Pembentukan</label>
												<input type="text" name="pembentukan" id="pembentukan" class="form-control" value="">
											</div>
											<div class="col-sm-3">
												<label class="validationCustom01">Pendewasaan</label>
												<input type="text" name="pendewasaan" id="pendewasaan" class="form-control" value="">
											</div>
											<div class="col-sm-3">
												<label class="validationCustom01">Pematangan</label>
												<input type="text" name="pematangan" id="pematangan" class="form-control" value="">
											</div>
										</div>
										<div class="row form-group">
											<div class="col-sm-12">
												<label for="validationCustom01">Pembinaan Khusus</label>
												<input type="text" name="pembinaan_khusus" id="pembinaan_khusus" class="form-control" readonly>
											</div>
										</div>
										<div class="row form-group">
											<div class="col-sm-12">
												<label for="validationCustom01">Masa Pembinaan</label>
												<input type="text" name="masa_pembinaan" id="masa_pembinaan" class="form-control" readonly>
											</div>
										</div>
										<div class="row form-group">
											<div class="col-sm-12">
												<label for="validationCustom01">Catatan</label>
												<input type="text" name="catatan" id="catatan" class="form-control" readonly>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="submit" class="btn-sm btn-rounded btn-info" id="save_pelanggaran" value="ins">Save</button>
										<button type="button" class="btn-sm btn-rounded btn-default" data-dismiss="modal" id="myBtn">Close</button>
									</div>
								</form>
							</div>
							{{-- tab2 --}}
							{{-- tab2 --}}
							{{-- tab2 --}}
							<div class="tab-pane fade" id="outline-two" role="tabpanel" aria-labelledby="tab-outline-two">
								{{-- <h3>List Pelanggaran</h3> --}}
								<h5>Total Poin</h5>
								<div class="form-group row">
									<div class="col-sm-3">
										<label class="validationCustom01">Orientasi</label>
										<input type="text" id="t_orientasi" name="t_orientasi" readonly value="" class="form-control">
									</div>
									<div class="col-sm-3">
										<label class="validationCustom01">Pembentukan</label>
										<input type="text" id="t_pembentukan" name="t_pembentukan" readonly value="" class="form-control">
									</div>
									<div class="col-sm-3">
										<label class="validationCustom01">Pendewasaan</label>
										<input type="text" id="t_pendewasaan" name="t_pendewasaan" readonly value="" class="form-control">
									</div>
									<div class="col-sm-3">
										<label class="validationCustom01">Pematangan</label>
										<input type="text" id="t_pematangan" name="t_pematangan" readonly value="" class="form-control">
									</div>
								</div>
								<div class="table-responsive">
									<table class="stripe row-border order-column">
										<thead>
											<tr>
												<th>Tingkat</th>
												<th>Jenis Pelanggaran</th>
												<th>Orientasi</th>
												<th>Pembentukan</th>
												<th>Pendewasaan</th>
												<th>Pematangan</th>
												@if (Auth::User()->role == 'super admin' || Auth::User()->role == 'admin')
												<th>Action</th>
												@endif
											</tr>
										</thead>
										<tbody id="data-user-pelangrn">
											{{-- data pelanggaran tampil disini.! --}}
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				{{-- </div> --}}
			</div>
		</div>
	</div>
</div>