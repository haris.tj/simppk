@include('new_layout.header')
@include('new_layout.sidebar')
<div class="dashboard-wrapper">
  <div class="container-fluid dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
      <!-- ============================================================== -->
      <!-- data table multiselects  -->
      <!-- ============================================================== -->
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

        <div class="card">
          <div class="card-header">
            @if(session('errors'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              Something it's wrong:
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif
            @if (Session::has('success'))
            <div class="alert alert-success">
              {{ Session::get('success') }}
            </div>
            @endif
            @if (Session::has('error'))
            <div class="alert alert-danger">
              {{ Session::get('error') }}
            </div>
            @endif
            <div class="form-group row">
              <div class="col-sm-6">
                <div class="float-left">
                  <button class="btn btn-sm btn-primary btn-rounded" id="tambah-pel">Tambah Pelanggaran</button>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="float-right">
                  <h3>Master Data Pelanggaran</h3>
                </div>
              </div>
            </div>
          </div>
          <div  class="card-body">
            {{-- tambah form muncul disini. --}}
            <form id="form-pelanggaran" method="POST" action="#">

            </form>
            {{-- end dari tambah from --}}
            <div class="table-responsive">
              <br>
              <table id="ms_prestasi"  class="stripe row-border order-column" style="width:100%">
                <thead>
                  <tr>
                    <th>Tingkat</th>
                    <th>Pelanggaran</th>
                    <th>Orientasi</th>
                    <th>Pembentukan</th>
                    <th>Pendewasaan</th>
                    <th>Pematangan</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data as $dt)
                  <tr>
                    <td>{{$dt->tingkat}}</td>
                    <td>{{\Illuminate\Support\Str::limit($dt->jenis_pelanggaran,50)}}</td>
                    <td>{{$dt->tahap_orientasi}}</td>
                    <td>{{$dt->tahap_pembentukan}}</td>
                    <td>{{$dt->tahap_pendewasaan}}</td>
                    <td>{{$dt->tahap_pematangan}}</td>
                    <td>
                      @include('admin.new_pelanggaran.delete')
                      <button class="btn-xs btn-rounded btn-warning" onclick="update({{$dt->id_pelanggaran}})">
                        <i class="ti-pencil"></i>
                      </button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- ============================================================== -->
        <!-- end data table multiselects  -->
        <!-- ============================================================== -->
      </div>
      <!-- ============================================================== -->
      <!-- end pageheader -->
      <!-- ============================================================== -->

    </div>
    @include('new_layout.footer')
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        $('#tambah-pel').click(function(event) {
          $.ajax({
            url: '{{url('pelanggaran/html')}}',
            type: 'GET',   
            success: function(data){
              $('#form-pelanggaran').attr('action', '{{url('pelanggaran/insert')}}');
              $('#form-pelanggaran').html(data.html)
            }
          });
        });
      });
      function update(id) {
        jQuery(document).ready(function($) {
         $.ajax({
          url: '{{url('pelanggaran/html')}}',
          type: 'GET',   
          success: function(data){
            $('#form-pelanggaran').attr('action', '{{url('pelanggaran/update')}}');
            $('#form-pelanggaran').html(data.html)
          }
        });
         $.ajax({
          url: '{{url('pelanggaran/view/')}}'+'/'+id,
          type: 'GET',
          dataType: 'json',
          success: function(data){
            $('#id_pel').val(data.id_pelanggaran)
            $('#tingkat').val(data.tingkat)
            $('#pelanggaran').val(data.jenis_pelanggaran)
            $('#orientasi').val(data.tahap_orientasi)
            $('#pembentukan').val(data.tahap_pembentukan)
            $('#pendewasaan').val(data.tahap_pendewasaan)
            $('#pematangan').val(data.tahap_pematangan)
          }
        });
       });
      }
    </script>