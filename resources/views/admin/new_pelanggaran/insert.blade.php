@csrf
<input type="hidden" name="id_pel" id="id_pel">
<div class="form-group row">
	<div class="col-sm-4">
		<label>Tingkat</label>
		<select class="form-control-sm form-control" id="tingkat" name="tingkat" required>
			<option value="">Pilih</option>
			<option value="utama">Utama</option>
			<option value="pertama">Pertama</option>
			<option value="kedua">Kedua</option>
			<option value="ketiga">Ketiga</option>
			<option value="keempat">Keempat</option>
		</select>
	</div>
	<div class="col-sm-8">
		<label>Pelanggaran</label>
		<input type="text" name="pelanggaran" id="pelanggaran" class="form-control-sm form-control" required placeholder="Masukan Pelanggaran">
	</div>
</div>
<div class="form-group row">
	<div class="col-sm-3">
		<label>Orientasi</label>
		<input type="number" name="orientasi" id="orientasi" class="form-control-sm form-control" required placeholder="0">
	</div>
	<div class="col-sm-3">
		<label>Pembentukan</label>
		<input type="number" name="pembentukan" id="pembentukan" class="form-control-sm form-control" required placeholder="0">
	</div>
	<div class="col-sm-3">
		<label>Pendewasaan</label>
		<input type="number" name="pendewasaan" id="pendewasaan" class="form-control-sm form-control" required placeholder="0">
	</div>
	<div class="col-sm-3">
		<label>Pematangan</label>
		<input type="number" name="pematangan" id="pematangan" class="form-control-sm form-control" required placeholder="0">
	</div>
</div>
<div class="float-right">
	<button class="btn btn-sm btn-secondary btn-rounded">Simpan</button>
</div>