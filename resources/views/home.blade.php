@include('new_layout.header')
@include('new_layout.sidebar')
<div class="dashboard-wrapper">
  <div class="container-fluid dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
      <!-- ============================================================== -->
      <!-- data table multiselects  -->
      <!-- ============================================================== -->
      <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

        <div class="card">
          <div class="card-header">
            Welcome {{Auth::user()->name}}
          </div>
          <div  class="card-body">
            <div class="row form-group d-flex justify-content-center">
              {{--  --}}
              <div class="col-md-12 col-lg-4 col-sm-6">
                <div class="card comp-card">
                  <div class="card-body">
                    <div class="row align-items-center">
                      <div class="col">
                        <h6 class="m-b-25">User</h6>
                        <h3 class="f-w-700 text-c-blue">{{$user}}</h3>
                        {{-- <p class="m-b-0">May 23 - June 01 (2017)</p> --}}
                      </div>
                      <div class="col-auto">
                        <i class="fas fa-users bg-primary"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {{--  --}}
              <div class="col-md-12 col-lg-4 col-sm-6">
                <div class="card comp-card">
                  <div class="card-body">
                    <div class="row align-items-center">
                      <div class="col">
                        <h6 class="m-b-25">Progdi</h6>
                        <h3 class="f-w-700 text-c-blue">{{$prd}}</h3>
                        {{-- <p class="m-b-0">May 23 - June 01 (2017)</p> --}}
                      </div>
                      <div class="col-auto">
                        <i class="fas fa-eye bg-warning"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {{--  --}}
              <div class="col-md-12 col-lg-4 col-sm-6">
                <div class="card comp-card">
                  <div class="card-body">
                    <div class="row align-items-center">
                      <div class="col">
                        <h6 class="m-b-25">Kelas</h6>
                        <h3 class="f-w-700 text-c-blue">{{$kls}}</h3>
                        {{-- <p class="m-b-0">May 23 - June 01 (2017)</p> --}}
                      </div>
                      <div class="col-auto">
                        <i class="fas fa-building bg-success"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <a href="{{ route('logout') }}" class="btn btn-danger btn-sm btn-rounded">Logout</a>
          </div>
        </div>
        <!-- ============================================================== -->
        <!-- end data table multiselects  -->
        <!-- ============================================================== -->
      </div>
      <!-- ============================================================== -->
      <!-- end pageheader -->
      <!-- ============================================================== -->

    </div>
    <!-- Button trigger modal -->
   {{--  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_pel">
      Launch demo modal
    </button>
    --}}
    <!-- Modal -->
    {{-- {{$data}} --}}
    @include('modal_home')
    @include('new_layout.footer')
    <script type="text/javascript">
      function detail(id){
        window.location = "/admin/komisioner/detail/"+id;
      }
      function tambah(){
        window.location = "/admin/komisioner/insert";
      }
      function download(){
        window.location = '/admin/kritik-saran/download/excel';
        target = "_blank";
      }
    </script>
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        $('#modal_pel').modal('show');
      });
    </script>
    <script type="text/javascript">
      function email(id) {
        window.location = '{{url('kirimemail')}}'+'/'+id;
      }
    </script>