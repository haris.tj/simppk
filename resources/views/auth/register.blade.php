<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up</title>
    <!-- Font Icon -->
    <link rel="stylesheet" href="assetz/login/fonts/material-icon/css/material-design-iconic-font.min.css">
    <!-- Main css -->
    <link rel="stylesheet" href="assetz/login/css/style.css">
    <style type="text/css">
        .signup-content {
            padding: 75px 0;
            background-color: green;
            border-radius: 10px;
        }
        label {
            position: absolute;
            left: 10px;
            top: 50%;
        }
        input {
            width: 100%;
            display: block;
            border: none;
            border-bottom: 1px solid #999;
            padding: 6px 30px;
            font-family: Poppins;
            box-sizing: border-box;
            border-radius: 8px;
        }
         .form-title {
            margin-bottom: 33px;
            color: white;
        }
    </style>
</head>
<body>

    <div class="container">
        <div class="signup-content">
            <div class="signup-form">
                <h2 class="form-title">Sign up</h2>
                <form method="POST" class="register-form" id="register-form" action="{{ url('register') }}">
                    @csrf
                    <div class="form-group">
                        <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                        <input type="text" name="name" id="name" placeholder="Your Name"/>
                    </div>
                    <div class="form-group">
                        <label for="email"><i class="zmdi zmdi-email"></i></label>
                        <input type="email" name="email" id="email" placeholder="Your Email"/>
                    </div>
                    <div class="form-group">
                        <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                        <input type="password" name="password" id="pass" placeholder="Password"/>
                    </div>
                    <div class="form-group">
                        <label for="re-pass"><i class="zmdi zmdi-lock-outline"></i></label>
                        <input type="password" name="password_confirmation" id="re_pass" placeholder="Repeat your password"/>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" />
                        <label for="agree-term" class="label-agree-term" style="color: white"><span><span></span></span>I agree all statements in  <a href="#" class="term-service" style="color: white">Terms of service</a></label>
                    </div>
                    @if(session('errors'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="color: white">
                        Something it's wrong:
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul style="color: white">
                            @foreach ($errors->all() as $error)
                            <li style="color: white">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="form-group form-button">
                        <input type="submit" name="signup" id="signup" class="form-submit" value="Register"/>
                    </div>
                </form>
            </div>
            <div class="signup-image">
                <figure><img src="assetz/images/logo-ppk.png" alt="sing up image"></figure>
                <a href="{{ route('login') }}" class="signup-image-link" style="color: white">I am already member</a>
            </div>
        </div>
    </div>

    <!-- JS -->
    <script src="assetz/login/vendor/jquery/jquery.min.js"></script>
    <script src="assetz/login/js/main.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('#email').change(function(event) {
            var email = $('#email').val()
            if (email != "" || email != 0) {
                if (!isValidEmailAddress(email)) {
                    alert('Email Tidak Valid')
                    $('#register-form').attr('action', '#');
                }else{
                    $('#register-form').attr('action', '{{ url('register') }}');
                }
            }
        });
    });
    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        return pattern.test(emailAddress);
    }
</script>