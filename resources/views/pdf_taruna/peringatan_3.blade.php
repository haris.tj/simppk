<html>

<head>
	<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
	<meta name=Generator content="Microsoft Word 14 (filtered)">
	<style>
		@page {
			margin: 0cm 0cm;
		}

		/** Define now the real margins of every page in the PDF **/
		body {
			margin-top: 3cm;
			margin-left: 2cm;
			margin-right: 2cm;
			margin-bottom: 2cm;
		}

		/** Define the header rules **/
		header {
			position: fixed;
			top: 0cm;
			left: 0cm;
			right: 0cm;
			height: 3cm;
		}

		/** Define the footer rules **/
		footer {
			position: fixed; 
			bottom: 0cm; 
			left: 0cm; 
			right: 0cm;
			height: 2cm;
		}
		.alignleft {
			float: left;
		}
		.alignright {
			float: right;
		}
	</style>
	<style>
		<!--
		/* Font Definitions */
		@font-face
		{font-family:Calibri;
			panose-1:2 15 5 2 2 2 4 3 2 4;}
			@font-face
			{font-family:Tahoma;
				panose-1:2 11 6 4 3 5 4 4 2 4;}
				/* Style Definitions */
				p.MsoNormal, li.MsoNormal, div.MsoNormal
				{margin-top:0cm;
					margin-right:0cm;
					margin-bottom:10.0pt;
					margin-left:0cm;
					line-height:115%;
					font-size:11.0pt;
					font-family:"Calibri","sans-serif";}
					p.MsoHeader, li.MsoHeader, div.MsoHeader
					{mso-style-link:"Header Char";
					margin:0cm;
					margin-bottom:.0001pt;
					font-size:11.0pt;
					font-family:"Calibri","sans-serif";}
					p.MsoFooter, li.MsoFooter, div.MsoFooter
					{mso-style-link:"Footer Char";
					margin:0cm;
					margin-bottom:.0001pt;
					font-size:11.0pt;
					font-family:"Calibri","sans-serif";}
					p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
					{mso-style-link:"Balloon Text Char";
					margin:0cm;
					margin-bottom:.0001pt;
					font-size:8.0pt;
					font-family:"Tahoma","sans-serif";}
					p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
					{margin-top:0cm;
						margin-right:0cm;
						margin-bottom:10.0pt;
						margin-left:36.0pt;
						line-height:115%;
						font-size:11.0pt;
						font-family:"Calibri","sans-serif";}
						p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
						{margin-top:0cm;
							margin-right:0cm;
							margin-bottom:0cm;
							margin-left:36.0pt;
							margin-bottom:.0001pt;
							line-height:115%;
							font-size:11.0pt;
							font-family:"Calibri","sans-serif";}
							p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
							{margin-top:0cm;
								margin-right:0cm;
								margin-bottom:0cm;
								margin-left:36.0pt;
								margin-bottom:.0001pt;
								line-height:115%;
								font-size:11.0pt;
								font-family:"Calibri","sans-serif";}
								p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
								{margin-top:0cm;
									margin-right:0cm;
									margin-bottom:10.0pt;
									margin-left:36.0pt;
									line-height:115%;
									font-size:11.0pt;
									font-family:"Calibri","sans-serif";}
									span.HeaderChar
									{mso-style-name:"Header Char";
									mso-style-link:Header;}
									span.FooterChar
									{mso-style-name:"Footer Char";
									mso-style-link:Footer;}
									span.BalloonTextChar
									{mso-style-name:"Balloon Text Char";
									mso-style-link:"Balloon Text";
									font-family:"Tahoma","sans-serif";}
									.MsoChpDefault
									{font-family:"Calibri","sans-serif";}
									.MsoPapDefault
									{margin-bottom:10.0pt;
										line-height:115%;}
										/* Page Definitions */
										@page WordSection1
										{size:595.3pt 841.9pt;
											margin:72.0pt 72.0pt 72.0pt 72.0pt;}
											div.WordSection1
											{page:WordSection1;}
											/* List Definitions */
											ol
											{margin-bottom:0cm;}
											ul
											{margin-bottom:0cm;}
											-->
										</style>

									</head>

									<header>
										<img src="https://i.postimg.cc/SNwfgZZ0/1.png"/>
									</header>

									<footer>
										<img src="https://i.postimg.cc/DzhdTSkB/2.png"/>
									</footer>

									<!-- Wrap the content of your PDF inside a main tag -->
									<main>
										<br><br><br><br>

										<body lang=IN style='tab-interval:36.0pt'>

											<div class=WordSection1>

												<p class=MsoNormal><span style='font-size:12.0pt;line-height:115%;font-family:
												"Times New Roman","serif"'>&nbsp;</span></p>

												<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=659
												style='width:494.45pt;border-collapse:collapse;mso-yfti-tbllook:1184;
												mso-padding-alt:0cm 0cm 0cm 0cm'>
												<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
													<td width=84 valign=top style='width:62.8pt;padding:0cm 5.4pt 0cm 5.4pt'>
														<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
														normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Nomor</span></p>
														<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
														normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Klasifikasi</span></p>
														<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
														normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Lampiran</span></p>
														<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
														normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Perihal</span></p>
													</td>
													<td width=320 valign=top style='width:240.3pt;padding:0cm 5.4pt 0cm 5.4pt'>
														<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
														normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>:
														-</span></p>
														<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
														normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>:
														Segera</span></p>
														<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
														normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>:
														-</span></p>
														<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
														normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>:
														Surat Pemberitahuan</span></p>
													</td>
													<td width=255 valign=top style='width:191.35pt;padding:0cm 5.4pt 0cm 5.4pt'>
														<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
														margin-left:36.0pt;margin-bottom:.0001pt;line-height:normal'><span
														style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Surabaya, {{date('d M Y')}}</span></p>
														<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
														normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
														<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
														margin-left:36.0pt;margin-bottom:.0001pt;line-height:normal'><span
														style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Kepada</span></p>
														<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
														normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Yth:&nbsp;&nbsp;&nbsp;&nbsp;
														Orang Tua / Wali Taruna</span></p>
														<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
														text-align:center;line-height:normal'><span style='font-size:12.0pt;
														font-family:"Times New Roman","serif"'>{{$ortu->nama_ortu}}</span></p>
														<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
														margin-left:36.0pt;margin-bottom:.0001pt;line-height:normal'><span
														style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Di</span></p>
														<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
														margin-left:36.0pt;margin-bottom:.0001pt;line-height:normal'><span
														style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
														<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
														margin-left:36.0pt;margin-bottom:.0001pt;line-height:normal'><span
														style='font-size:12.0pt;font-family:"Times New Roman","serif"'>T EM P A T</span></p>
													</td>
												</tr>
											</table>

											<p class=MsoNormal style='text-align:justify'><span style='font-size:12.0pt;
											line-height:115%;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

											<p class=MsoListParagraph style='text-align:justify;text-indent:-18.0pt'><span
												style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>1.</span><span
												style='font-size:7.0pt;line-height:115%;font-family:"Times New Roman","serif"'>&nbsp;
											</span><span style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Berdasarkan
												hasil Berita Acara Pengambilan Keterangan Perwira Batalyon Pusat Pembangunan
												Karakter&nbsp; Taruna dan&nbsp;&nbsp; Pasis, di dapati bahwa bahwa&nbsp;
											taruna:</span></p>

										{{-- 	<p class=MsoListParagraph style='text-align:justify'><span style='font-size:
											12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>&nbsp;</span></p> --}}

											<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
											style='margin-left:72.0pt;border-collapse:collapse;mso-yfti-tbllook:1184;
											mso-padding-alt:0cm 0cm 0cm 0cm'>
											<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
												<td width=147 valign=top style='width:110.6pt;padding:0cm 5.4pt 0cm 5.4pt'>
													<p class=MsoListParagraph style='margin:0cm;margin-bottom:.0001pt;text-align:
													justify;line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Nama</span></p>
													<p class=MsoListParagraph style='margin:0cm;margin-bottom:.0001pt;text-align:
													justify;line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Jurusan</span></p>
													<p class=MsoListParagraph style='margin:0cm;margin-bottom:.0001pt;text-align:
													justify;line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Semester
													/ Program</span></p>
												</td>
												<td width=373 valign=top style='width:279.5pt;padding:0cm 5.4pt 0cm 5.4pt'>
													<p class=MsoListParagraph style='margin:0cm;margin-bottom:.0001pt;text-align:
													justify;line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>:
													{{$detail->nama}}</span></p>
													<p class=MsoListParagraph style='margin:0cm;margin-bottom:.0001pt;text-align:
													justify;line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>:
													-</span></p>
													<p class=MsoListParagraph style='margin:0cm;margin-bottom:.0001pt;text-align:
													justify;line-height:normal'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>:
													{{$detail->semester}} / {{$detail->program}}</span></p>
												</td>
											</tr>
										</table>

										<p class=MsoNormal style='margin-left:36.0pt;text-align:justify'><span
											style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

											<p class=MsoNormal style='margin-left:36.0pt;text-align:justify'><span
												style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Pada&nbsp;
												tanggal {{$pel->waktu}} telah melakukan pelanggaran terhadap
												Pertibtar sesuai dengan pasal ....ayat....tingkat....dengan&nbsp; jenis
											pelanggaran yaitu {{$pel->pelanggaran}}<span></p>

											<p class=MsoListParagraph style='text-align:justify;text-indent:-18.0pt'><span
												style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>2.</span><span
												style='font-size:7.0pt;line-height:115%;font-family:"Times New Roman","serif"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											</span><span style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Sehubungan
											dengan hal tersebut diatas, maka bersama dengan ini kami sampaikan :</span></p>

											<p class=MsoListParagraph style='margin-left:72.0pt;text-align:justify;
											text-indent:-18.0pt'><span style='font-size:12.0pt;line-height:115%;font-family:
											"Times New Roman","serif"'>a.</span><span style='font-size:7.0pt;line-height:
											115%;font-family:"Times New Roman","serif"'>&nbsp;&nbsp;&nbsp;
										</span><span style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Undangan Orang Tua/Wali Untuk hadir ke Pusat Pembangunan Karakter  untuk membahas  terkait pelanggaran yang telah dilakukan  taruna.</span></p>

										<p class=MsoListParagraph style='margin-left:72.0pt;text-align:justify;
										text-indent:-18.0pt'><span style='font-size:12.0pt;line-height:115%;font-family:
										"Times New Roman","serif"'>b.</span><span style='font-size:7.0pt;line-height:
										115%;font-family:"Times New Roman","serif"'>&nbsp;&nbsp;&nbsp; </span><span
										style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Untuk
										konfirmasi kesediaanya melakukan video call atau&nbsp; kedatangannya di mohon
										untuk menghubungi Bapak {{$boss->nama}} sebagai Perwira
									Batalyon di nomor HP {{$boss->telepon}}</span></p>

									<p class=MsoListParagraph style='text-align:justify;text-indent:-18.0pt'><span
										style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>3.</span><span
										style='font-size:7.0pt;line-height:115%;font-family:"Times New Roman","serif"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</span><span style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Demikian
									atas perhatian dan kerjasamanya&nbsp; kami ucapkan terima kasih.</span></p>

                   <!--  <p class=MsoListParagraph style='text-align:justify'><b><span style='font-size:
                   	12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>&nbsp;<o:p></o:p></span></b></p> -->

                   	<p class=MsoListParagraph align=right style='text-align:right'><b><span
                   		style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>a.n.
                   	DIREKTUR POLTEKPEL SURABAYA</span></b></p>

                   	<p class=MsoListParagraph><b><span
                   		style='margin-left: 370px;font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>
                   	WAKIL DIREKTUR III</span></b></p>

                   	{{-- <p class=MsoListParagraph align=right style='text-align:right'><span
                   		style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>&nbsp;</span></p>
 --}}
                   		<p class=MsoListParagraph align=right style='text-align:right'><span
                   			style='font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

                   			<p class=MsoListParagraph><b><u><span
                   				style='margin-left:310px; font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>{{$boss->nama_direktur}}</span></u></b></p>

                   			<p class=MsoListParagraph><span
                   				style='margin-left:380px;font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>{{$boss->penata}}</span></p>

                   			<p class=MsoListParagraph><span
                   				style='margin-left:360px;font-size:12.0pt;line-height:115%;font-family:"Times New Roman","serif"'>NIP.
                   			{{$boss->nip}}</span></p>

                   		</div>

                   	</body>

                   	</html>

                   </main>
