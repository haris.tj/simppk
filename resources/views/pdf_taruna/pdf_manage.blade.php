@include('new_layout.header')
@include('new_layout.sidebar')
<div class="dashboard-wrapper">
	<div class="container-fluid dashboard-content">
		<!-- ============================================================== -->
		<!-- pageheader -->
		<!-- ============================================================== -->
		<div class="row">
			<!-- ============================================================== -->
			<!-- data table multiselects  -->
			<!-- ============================================================== -->
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

				<div class="card">
					<div class="card-header">
						@if(session('errors'))
						<div class="alert alert-danger alert-dismissible fade show" role="alert">
							Something it's wrong:
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						@if (Session::has('success'))
						<div class="alert alert-success">
							{{ Session::get('success') }}
						</div>
						@endif
						@if (Session::has('error'))
						<div class="alert alert-danger">
							{{ Session::get('error') }}
						</div>
						@endif
						<div class="row form-group">
							<div class="col-sm-6">
								<button class="btn btn-sm btn-rounded btn-primary" id="preview">Preview PDF</button>
							</div>
							<div class="col-sm-6 text-right">
								<h3>Template PDF</h3>
							</div>
						</div>
					</div>
					<div class="card-body">
						<form id="from-pdf" method="POST" action="{{url('pdf/update')}}">
							@csrf
							<div class="form-group row">
								<div class="col-sm-6">
									<label>Nama Perwira</label>
									<input type="text" name="nama_perwira" id="nama_perwira" class="form-control" placeholder="Nama Perwira" required value="{{$data->nama}}">
								</div>
								<div class="col-sm-6">
									<label>Nomor Tlp Perwira</label>
									<input type="number" name="tlp_perwira" id="tlp_perwira" class="form-control" placeholder="081225704000" required value="{{$data->telepon}}">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-4">
									<label>Nama Direktur</label>
									<input type="text" name="nama_direktur" id="nama_direktur" class="form-control" placeholder="Nama Direktur" required value="{{$data->nama_direktur}}">
								</div>
								<div class="col-sm-4">
									<label>Devisi</label>
									<input type="text" name="penata" id="penata" class="form-control" placeholder="Penata Tk.I(III/d)" required value="{{$data->penata}}">
								</div>
								<div class="col-sm-4">
									<label>NIP</label>
									<input type="number" name="nip" id="nip" class="form-control" placeholder="1981x2152xx212xxxx" required value="{{$data->nip}}">
								</div>
							</div>
							<div class="float-md-right">
								<button class="btn btn-rounded btn-secondary btn-sm">Update</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- ============================================================== -->
			<!-- end data table multiselects  -->
			<!-- ============================================================== -->
		</div>
		<!-- ============================================================== -->
		<!-- end pageheader -->
		<!-- ============================================================== -->

	</div>
	@include('new_layout.footer')
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('#preview').click(function(event) {
				window.location = '{{url('pdf/preview')}}'
			});
		});
	</script>