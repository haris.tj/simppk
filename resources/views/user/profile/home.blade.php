               @include('new_layout.header')
               @include('new_layout.sidebar')
               <div class="dashboard-wrapper">
                  <div class="dashboard-ecommerce">
                     <div class="container-fluid dashboard-content ">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                           <div class="card">

                              <div class="card-header">
                                @if(session('errors'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                 Something it's wrong:
                                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                 </button>
                                 <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                 </ul>
                              </div>
                              @endif
                              @if (Session::has('success'))
                              <div class="alert alert-success">
                                 {{ Session::get('success') }}
                              </div>
                              @endif
                              @if (Session::has('error'))
                              <div class="alert alert-danger">
                                 {{ Session::get('error') }}
                              </div>
                              @endif
                           </div>
                           <div class="card-body">
                              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
                                 {{-- <div class="section-block"> --}}
                                    <h5 class="section-title">Lengkapi Data</h5>
                                   {{--  <p>Takes the basic nav from above and adds the .nav-tabs class to generate a tabbed interface..</p> --}}
                                 {{-- </div> --}}
                                 <div class="tab-outline">
                                    <ul class="nav nav-tabs" id="myTab2" role="tablist">
                                       <li class="nav-item">
                                          <a class="nav-link active" id="tab-outline-one" data-toggle="tab" href="#outline-one" role="tab" aria-controls="home" aria-selected="true">Tab#1</a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" id="tab-outline-two" data-toggle="tab" href="#outline-two" role="tab" aria-controls="profile" aria-selected="false">Tab#2</a>
                                       </li>
                                       <li class="nav-item">
                                          <a class="nav-link" id="tab-outline-three" data-toggle="tab" href="#outline-three" role="tab" aria-controls="contact" aria-selected="false">Tab#3</a>
                                       </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent2">
                                       <div class="tab-pane fade show active" id="outline-one" role="tabpanel" aria-labelledby="tab-outline-one">
                                          <form action="/user/data/insert1" method="post" enctype="multipart/form-data" role="form">
                                             <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                             <div class="form-group row">
                                                <div class="col-md-6">
                                                   <label for="validationCustom01">Nama Lengkap</label>
                                                   <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap">
                                                   <div class="valid-feedback">
                                                      Looks good!
                                                   </div>
                                                </div>
                                                <div class="col-md-6">
                                                   <label for="validationCustom01">Nomer Induk Taruna</label>
                                                   <input type="number" class="form-control" id="nit" name="nit" placeholder="Nomor Induk Taruna" required>
                                                   <div class="valid-feedback">
                                                      Looks good!
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="form-group row">
                                                <div class="col-md-4">
                                                   <label for="validationCustom01">Angkatan / Tahun Masuk</label>
                                                   <input type="number" class="form-control" id="tahun" name="tahun" placeholder="Angkatan / Tahun Angkatan" readonly>
                                                   <div class="valid-feedback">
                                                      Looks good!
                                                   </div>
                                                </div>
                                                <div class="col-md-4">
                                                   <label for="validationCustom01">Program</label>
                                                   <input type="text" class="form-control" id="program" name="program" placeholder="Program" required readonly>
                                                   <div class="valid-feedback">
                                                      Looks good!
                                                   </div>
                                                </div>
                                                <div class="col-md-4">
                                                   <label for="validationCustom01">Semester</label>
                                                   <select class="form-control" id="semester" name="semester">
                                                      <option value="">Pilih</option>
                                                      <option value="1">1</option>
                                                      <option value="2">2</option>
                                                      <option value="3">3</option>
                                                      <option value="4">4</option>
                                                      <option value="5">5</option>
                                                      <option value="6">6</option>
                                                      <option value="7">7</option>
                                                      <option value="8">8</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="form-group row">
                                                <div class="col-md-4">
                                                   <label for="validationCustom01">Kelas</label>
                                                   <select name="kelas" id="inpKelas" class="form-control">
                                                      {{-- list kelas --}}
                                                   </select>
                                                </div>
                                                <div class="col-md-4">
                                                   <label for="validationCustom01">Tempat Lahir</label>
                                                   <input type="text" class="form-control" id="tempat" name="tempat" placeholder="Tempat Lahir" required>
                                                   <div class="valid-feedback">
                                                      Looks good!
                                                   </div>
                                                </div>
                                                <div class="col-md-4">
                                                   <label for="validationCustom01">Tanggal Lahir</label>
                                                   <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" placeholder="" required>
                                                   <div class="valid-feedback">
                                                      Looks good!
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="form-group row">
                                                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                             </div>
                                          </form>
                                       </div>
                                       <div class="tab-pane fade" id="outline-two" role="tabpanel" aria-labelledby="tab-outline-two">
                                         <form action="/user/data/insert2" method="post" enctype="multipart/form-data" role="form">
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                          <div class="form-group row">
                                             <div class="col-md-12">
                                                <label for="validationCustom01">Nama Orangtua / Wali</label>
                                                <input type="text" class="form-control" id="ortu" name="ortu" placeholder="Nama Orangtua / Wali">
                                                <div class="valid-feedback">Looks good!</div>
                                             </div>
                                          </div>
                                          <div class="form-group row">
                                             <div class="col-md-6">
                                                <label for="validationCustom01">Provinsi</label>
                                                <select name="prov" class="form-control" id="provinsi">
                                                   <option>Pilih Provinsi</option>
                                                   @foreach($data as $prov)
                                                   @echo '<option value="{{$prov->id}}">{{$prov->nama}}</option>';
                                                   @endforeach
                                                </select>
                                             </div>
                                             <div class="col-md-6">
                                                <label for="validationCustom01">Kabupaten</label>
                                                <select name="kab" class="form-control" id="kabupaten">
                                                   <option value="">Pilih Kabupaten</option>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="form-group row">
                                             <div class="col-md-6">
                                                <label for="validationCustom01">Kecamatan</label>
                                                <select name="kec" class="form-control" id="kecamatan">
                                                   <option>Select Kecamatan</option>
                                                </select>
                                             </div>
                                             <div class="col-md-6">
                                                <label for="validationCustom01">Desa</label>
                                                <select name="des" class="form-control" id="desa">
                                                   <option>Select Desa</option>
                                                </select>
                                             </div>
                                          </div>

                                          <div class="form-group row">
                                             <div class="col-md-4">
                                                <label for="validationCustom01">NoTelepon Orang Tua / Wali 1</label>
                                                <input type="number" class="form-control" id="notlportu1" name="notlportu1" placeholder="081119999999">
                                                <div class="valid-feedback">
                                                   Looks good!
                                                </div>
                                             </div>
                                             <div class="col-md-4">
                                                <label for="validationCustom01">NoTelepon Orang Tua / Wali 2</label>
                                                <input type="number" class="form-control" id="notlportu2" name="notlportu2" placeholder="081119999999">
                                                <div class="valid-feedback">
                                                   Looks good!
                                                </div>
                                             </div>
                                             <div class="col-md-4">
                                                <label for="validationCustom01">Email Orang Tua / Wali</label>
                                                <input type="email" class="form-control" id="emailortu" name="emailortu" placeholder="mail@mail.com">
                                                <div class="valid-feedback">
                                                   Looks good!
                                                </div>
                                             </div>
                                          </div>
                                          <div class="form-group row">
                                             <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                          </div>
                                       </form>
                                    </div>
                                    <div class="tab-pane fade" id="outline-three" role="tabpanel" aria-labelledby="tab-outline-three">
                                     <form action="/user/data/insert3" method="post" enctype="multipart/form-data" role="form">
                                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                       <div class="form-group row">
                                          <div class="col-md-6">
                                             <label for="validationCustom01">NoTelepon Taruna</label>
                                             <input type="number" class="form-control" id="notlptaruna" name="notlptaruna" placeholder="081119999999">
                                             <div class="valid-feedback">Looks good!</div>
                                          </div>
                                          <div class="col-md-6">
                                             <label for="validationCustom01">Email Taruna</label>
                                             <input type="email" class="form-control" id="emailtaruna" name="emailtaruna" placeholder="mail@mail.com">
                                             <div class="valid-feedback">
                                                Looks good!
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group row">
                                          <div class="col-md-4">
                                             <label for="validationCustom01">Facebook</label>
                                             <input type="text" class="form-control" id="facebook" name="facebook" placeholder="username">
                                             <div class="valid-feedback">
                                                Looks good!
                                             </div>
                                          </div>
                                          <div class="col-md-4">
                                             <label for="validationCustom01">Instagram</label>
                                             <input type="text" class="form-control" id="instagram" name="instagram" placeholder="username">
                                             <div class="valid-feedback">
                                                Looks good!
                                             </div>
                                          </div>
                                          <div class="col-md-4">
                                             <label for="validationCustom01">Twitter</label>
                                             <input type="text" class="form-control" id="twitter" name="twitter" placeholder="username">
                                             <div class="valid-feedback">
                                                Looks good!
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group row">
                                          <div class="col-md-6">
                                             <label for="validationCustom01">Ekstra Kurikuler</label> <br>
                                             <select class="form-control" id="ekstrakurikuler" name="ekstrakurikuler[]" multiple="multiple">
                                               <option value=""></option>
                                               <option value="Sepak Bola">Sepak Bola</option>
                                               <option value="Futsal">Futsal</option>
                                               <option value="Bola Voli">Bola Voli</option>
                                               <option value="Pencak Silat">Pencak Silat</option>
                                               <option value="Scuba Diving">Scuba Diving</option>
                                               <option value="Renang">Renang</option>
                                               <option value="Basket">Basket</option>
                                               <option value="Badminton/Bulu Tangkis">Badminton/Bulu Tangkis</option>
                                               <option value="Taekwondo">Taekwondo</option>
                                               <option value="Floor Ball">Floor Ball</option>
                                               <option value="Marching Band">Marching Band</option>
                                               <option value="Paduan Suara">Paduan Suara</option>
                                               <option value="Kolone Senapan">Kolone Senapan</option>
                                               <option value="Karawitan">Karawitan</option>
                                               <option value="Rebana/Hadrah">Rebana/Hadrah</option>
                                               <option value="Mengaji/Kajian Agama">Mengaji/Kajian Agama</option>
                                               <option value="Tari">Tari</option>
                                               <option value="Angklung">Angklung</option>
                                               <option value="Pedang sora">Pedang sora</option>
                                               <option value="Senam / Zummba">Senam / Zummba</option>
                                            </select>
                                            <div class="valid-feedback">
                                             Looks good!
                                          </div>
                                       </div>
                                       <div class="col-md-6">
                                          <label for="validationCustom01">Staff / Tarbi</label>
                                          <select name="staff" id="staff" class="form-control" required>
                                             <option>Pilih</option>
                                             <option value="staff">Staff</option>
                                             <option value="tarbi">Tarbi</option>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="form-group row">
                                       <div class="col-md-12" id="v_ket">

                                       </div>
                                    </div>
                                    <div class="form-group row">
                                       <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         {{-- </form> --}}
         @include('new_layout.footer')
         <script>
            $(document).ready(function() {
               $('#ekstrakurikuler').select2({
                  placeholder: "Pilih Ekstrakurikuler"
               });
               $("#provinsi").change(function() {
                  var url = "{{url('id/kabupaten/')}}/" + $(this).val();
                  $('#kabupaten').load(url);
                  return false;
               })

               $("#kabupaten").change(function() {
                  var url = "{{url('id/kecamatan/')}}/" + $(this).val();
                  $('#kecamatan').load(url);
                  return false;
               })

               $("#kecamatan").change(function() {
                  var url = "{{url('id/desa/')}}/" + $(this).val();
                  $('#desa').load(url);
                  return false;
               })
            });
         </script>
         <script type="text/javascript">
            jQuery(document).ready(function($) {
               $('#nit').change(function(event) {
                  /* Act on the event */
                  var data = $('#nit').val();
                  var thn = data.substr(2,2);
                  var prod = data.substr(8,2);

                  $('#tahun').val('20'+thn);

                  $.ajax({
                     url: '{{url('data/prodi')}}'+'/'+prod,
                     type: 'GET',
                     dataType: 'json',
                     success: function(response){         
                        $('#program').val(response.prodi);      
                     }
                  });                        
               });
               // $('#inpKelas').click(function(event) {
                  $.get('/wali', function(data) {
                     $.each(JSON.parse(data), function(index, val) {
                      $('#inpKelas').append(val)
                   });
                  });
                  $('#inpKelas').change(function(event) {
                     /* Act on the event */
                     // alert($(this).val())
                  });
               });
            // });
         </script>
         <script type="text/javascript">
            jQuery(document).ready(function($) {
             $('#staff').change(function(event) {
                if ($('#staff').val() == "staff") {
                 var html = ''
                 html += 
                 '<label for="validationCustom01">Keterangan</label>'+
                 '<textarea id="keterangan" name="keterangan" required class="form-control" rows="8"></textarea>';
                 $('#v_ket').html(html)
              }else{
                 $('#v_ket').remove()

              }
           });
          });
       </script>
    </div>
 </div>