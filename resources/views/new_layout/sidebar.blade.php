        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                Menu
                            </li>
                            @if(Auth::User()->role != "user")
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('dashboard')}}"><i class="fa fa-fw far fa-handshake"></i>Dashboard</a>
                            </li>
                            @endif
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('user/data')}}"><i class="fa fa-fw fas fa-user"></i>Data Taruna</a>
                            </li>
                            @if(Auth::User()->role == 'operator' || Auth::User()->role == 'super admin')
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('kelas')}}"><i class="fa fa-fw fas fa-building"></i>Kelas Manage</a>
                            </li>
                            @endif
                            @if(Auth::user()->role == 'super admin' || Auth::user()->role == 'admin')
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-10" aria-controls="submenu-10"><i class="fas fa-f fa-folder"></i>Master Data</a>
                                <div id="submenu-10" class="collapse submenu" style="">
                                    <ul class="nav flex-column">
                                        @if(Auth::user()->role == 'super admin')
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{url('home')}}">User Manage</a>
                                        </li>
                                        @endif
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{url('prestasi')}}">Prestasi Manange</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{url('prodi')}}">Prodi Manange</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{url('pelanggaran')}}">Pelanggaran Manange</a>
                                        </li>
                                        @if(Auth::user()->role == 'super admin')
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{url('pdf')}}">PDF Manange</a>
                                        </li>
                                        @endif
                                    </ul>
                                </div>
                            </li>
                            @endif
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
    <!-- ============================================================== -->