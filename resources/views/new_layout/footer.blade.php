  <!-- ============================================================== -->
  <!-- footer -->
  <!-- ============================================================== -->
  <div class="footer">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
          Copyright &copy;<script>document.write(new Date().getFullYear());</script> SITAPOS.
        </div>
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
          <div class="text-md-right footer-links d-none d-sm-block">
            <a href="javascript: void(0);">About</a>
            <a href="javascript: void(0);">Support</a>
            <a href="javascript: void(0);">Contact Us</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ============================================================== -->
  <!-- end footer -->
  <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- end main wrapper -->
<!-- ============================================================== -->
<!-- Optional JavaScript -->
<script src="{{url('assetz/vendor/jquery/jquery-3.3.1.min.js')}}"></script>
<script src="{{url('assetz/vendor/bootstrap/js/bootstrap.bundle.js')}}"></script>
<script src="{{url('assetz/libs/js/main-js.js')}}"></script>


{{-- baru --}}
<script src="{{url('assetz/vendor/datatables/js/buttons.bootstrap4.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/fixedcolumns/3.3.2/js/dataTables.fixedColumns.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
{{-- <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script> --}}
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    var table = $('#example').DataTable({
      scrollX:        true,
    });
    $('#pelTable').DataTable();
    $('#tb_pelanggaran').DataTable();
    $('#ms_prestasi').DataTable();
    $('#ms_pelanggaran').DataTable();
  });
</script>
</body>
</html>