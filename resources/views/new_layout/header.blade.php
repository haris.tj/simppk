<!doctype html>
<html lang="en">


<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SITAPOS</title>
    <link rel="stylesheet" href="{{url('assetz/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link href="{{url('assetz/vendor/fonts/circular-std/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{url('assetz/libs/css/style.css')}}">
    <link rel="stylesheet" href="{{url('assetz/libs/css/waves.min.css')}}">
    <link rel="stylesheet" href="{{url('assetz/libs/css/chartist.css')}}">
    <link rel="stylesheet" href="{{url('assetz/libs/css/feather.css')}}">
    <link rel="stylesheet" href="{{url('assetz/libs/css/widget.css')}}">
    <link rel="stylesheet" href="{{url('assetz/vendor/fonts/fontawesome/css/fontawesome-all.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assetz/vendor/datatables/css/dataTables.bootstrap4.css')}}">
    {{-- <link rel="stylesheet" type="text/css" href="{{url('assetz/vendor/datatables/css/buttons.bootstrap4.css')}}"> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{url('assetz/vendor/datatables/css/select.bootstrap4.css')}}"> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{url('assetz/vendor/datatables/css/fixedHeader.bootstrap4.css')}}"> --}}
    <link rel="stylesheet" href="{{url('assetz/vendor/fonts/themify-icons/themify-icons.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.3.2/css/fixedColumns.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">


    
    {{-- https://cdn.datatables.net/fixedcolumns/3.3.2/css/fixedColumns.dataTables.min.css --}}

</head>
<style type="text/css">
    .pagination li{
        float: left;
        list-style-type: none;
        margin:8px;
    }
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
    
    div.container {
        width: 80%;
    }
</style>
<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-white fixed-top">
                <a class="navbar-brand" href="/admin">SITAPOS</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto navbar-right-top">
                        <li class="nav-item">
                            <div id="custom-search" class="top-search-bar">
                                <input class="form-control" type="text" placeholder="Search..">
                            </div>
                        </li>

                        <li class="nav-item dropdown nav-user">
                            <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{url('assetz/images/profile.png')}}" alt="" class="user-avatar-md rounded-circle"></a>
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                                <div class="nav-user-info">
                                    <h5 class="mb-0 text-white nav-user-name">{{Session::get('username')}}</h5>
                                </div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#updateAccount"><i class="fas fa-user mr-2"></i>Account</a>
                                <a class="dropdown-item" href="{{ route('logout') }}"><i class="fas fa-power-off mr-2"></i>Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->

        {{-- MODAL --}}

          {{--       <button class="btn-xs btn-rounded btn-warning" data-toggle="modal" data-target="#updateAccount">
                    <i class="ti-pencil"></i>
                </button> --}}
                <div class="modal fade" id="updateAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4>Profile</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times</button>
                            </div>
                            <form action="/acc/update" method="post" enctype="multipart/form-data" role="form">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="id" value="{{Auth::User()->id}}" id="id">
                                {{method_field('patch')}}
                                <div class="modal-body">
                                   <div class="card">
                                    <img src="{{url('assetz/images/profile.png')}}" class="card-img-top">
                                    <!-- .card-body -->
                                    <div class="card-body">
                                        <!-- /.user-avatar -->
                                        <div class="row from-group">
                                            <div class="col-sm-12">
                                                <label class="validationCustom01">Name</label>
                                                <input type="text" name="name" class="form-control" value="{{Auth::User()->name}}">
                                            </div>
                                        </div>
                                        <div class="row from-group">
                                            <div class="col-sm-12">
                                                <label class="validationCustom01">Email</label>
                                                <input type="email" name="email" class="form-control" value="{{Auth::User()->email}}">
                                            </div>
                                        </div>
                                        <div class="row from-group">
                                            <div class="col-sm-12">
                                                <label class="validationCustom01">Password</label>
                                                <input type="password" name="password" placeholder="New Password" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn-rounded btn-info btn-sm">Save</button>
                                <button type="button" class="btn-rounded btn-default btn-sm" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function reply_click()
                {   var id = event.srcElement.id;
                    var x = document.getElementById('id').value = id;
                }
            </script>
    {{-- END MODAL --}}