  @if(Auth::user()->role != 'user')
  <div class="modal fade" id="modal_pel" tabindex="-1" role="dialog" aria-labelledby="modal_pelLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg" role="document">
  		<div class="modal-content">
  			<div class="modal-header">
  				<h5 class="modal-title" id="modal_pelLabel">Pelanggaran</h5>
  				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  					<span aria-hidden="true">&times;</span>
  				</button>
  			</div>

  			<div class="modal-body">
          @if ($message = Session::get('success'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
          </div>
          @endif

          @if ($message = Session::get('error'))
          <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
          </div>
          @endif
          <div class="table-responsive">
           <table id="pelTable" class="table table-hover" style="width:100%">
            <thead>
             <tr>
              <th>NIT</th>
              <th>Tahap</th>
              <th>Poin Pelanggaran</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
           @foreach($data as $dt)
           <tr>
            <td>{{$dt->nit}}</td>
            <?php
            $a = $dt->orientasi;
            $b = $dt->pembentukan;
            $c = $dt->pendewasaan;
            $d = $dt->pematangan;
            $tingkat = 0;
            $point = $dt->orientasi;
            if ($a >= 50 && $a<75) {
             $tingkat = 1;
           }elseif ($a >= 75 && $a<100) {
             $tingkat = 2;
           }elseif ($a >= 100) {
             $tingkat = 3;
           }
           if($point > 100) {
             $point = 100;
           }
           ?>
           <td>{{$tingkat}}</td>
           <td>{{$point}}</td>
           <td>
             @if($dt->status == 1)
             <span class="badge-success badge">Terkirim</span>
             @else
             <span class="badge-danger badge">Belum Terkirim</span>	
             @endif
           </td>
           <td>
             @if($dt->status == 1)
             <button class="btn-sm btn-rounded btn-primary btn disabled">Email</button>
             @else
             <button class="btn-sm btn-rounded btn-primary btn" id="btnsendmail" onclick="email({{$dt->id_user}})">Email</button>
             @endif
           </td>
         </tr>
         @endforeach
       </tbody>
     </table>
   </div>
 </div>
 <div class="modal-footer">
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
@endif