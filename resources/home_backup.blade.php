@include('new_layout.header')
@include('new_layout.sidebar')
<div class="dashboard-wrapper">
    <div class="container-fluid dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- ============================================================== -->
            <!-- data table multiselects  -->
            <!-- ============================================================== -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

              <div class="card">
                  <div class="card-header">
                      Welcome {{Auth::user()->name}}
                  </div>
                  <div  class="card-body">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                  </div>
                  <div class="card-footer">
                      <a href="{{ route('logout') }}" class="btn btn-danger btn-sm btn-rounded">Logout</a>
                  </div>
              </div>
              <!-- ============================================================== -->
              <!-- end data table multiselects  -->
              <!-- ============================================================== -->
          </div>
          <!-- ============================================================== -->
          <!-- end pageheader -->
          <!-- ============================================================== -->

      </div>
      @include('new_layout.footer')
      <script type="text/javascript">
        function detail(id){
            window.location = "/admin/komisioner/detail/"+id;
        }
        function tambah(){
            window.location = "/admin/komisioner/insert";
        }
        function download(){
            window.location = '/admin/kritik-saran/download/excel';
            target = "_blank";
        }
    </script>