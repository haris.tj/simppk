<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataTarunasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_tarunas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_user',5);
            $table->string('nama',60);
            $table->string('nit',20);
            $table->string('wali',50);
            $table->integer('tahun');
            $table->string('program',80);
            $table->integer('semester',2);
            $table->string('kelas',30);
            $table->string('tempatlahir',30);
            $table->string('tgllahir',30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_tarunas');
    }
}
