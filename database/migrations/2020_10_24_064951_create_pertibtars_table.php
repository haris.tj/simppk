<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePertibtarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pertibtars', function (Blueprint $table) {
           $table->increments('id');
           $table->string('tingkat',30);
           $table->string('jenis',150);
           $table->integer('tahap_orientasi');
           $table->integer('tahap_pembentukan');
           $table->integer('tahap_pendewasaan');
           $table->integer('tahap_pematangan');
           $table->string('pembinaan_khusus',120);
           $table->string('masa_pembinaan',50);
           $table->string('catatan',100);
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pertibtars');
    }
}
