<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataOrangtuasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_orangtuas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_user',5);
            $table->string('nama_ortu',50);
            $table->string('provinsi',50);
            $table->string('kabupaten',50);
            $table->string('kecamatan',50);
            $table->string('desa',50);
            $table->string('tlp_ortu1',13);
            $table->string('tlp_ortu2',13);
            $table->string('email_ortu',30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_orangtuas');
    }
}
