<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_user',5);
            $table->string('nama',60);
            $table->string('nit',20);
            $table->string('nisn',20);
            $table->integer('tahun');
            $table->string('program',80);
            $table->string('jurusan',80);
            $table->string('kelas',30);
            $table->string('tempatlahir',30);
            $table->string('tgllahir',30);
            $table->string('nama_ortu',50);
            $table->string('provinsi',50);
            $table->string('kabupaten',50);
            $table->string('kecamatan',50);
            $table->string('desa',50);
            $table->string('tlp_ortu1',13);
            $table->string('tlp_ortu2',13);
            $table->string('email_ortu',30);
            $table->string('tlp_taruna',13);
            $table->string('email_taruna',30);
            $table->string('facebook',20);
            $table->string('instagram',20);
            $table->string('twitter',20);
            $table->text('ektra_kurikuler');
            $table->string('staff',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('user_models');
    }
}
