<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataPelanggransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_pelanggrans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->integer('id_pelanggaran');
            $table->integer('orientasi');
            $table->integer('pembentukan');
            $table->integer('pendewasaan');
            $table->integer('pematangan');
            $table->integer('semester');
            $table->string('waktu');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_pelanggrans');
    }
}
