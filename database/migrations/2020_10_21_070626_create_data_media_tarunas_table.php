<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataMediaTarunasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_media_tarunas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_user',5);
            $table->string('tlp_taruna',13);
            $table->string('email_taruna',30);
            $table->string('facebook',20);
            $table->string('instagram',20);
            $table->string('twitter',20);
            $table->text('ektra_kurikuler');
            $table->string('staff',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_media_tarunas');
    }
}
