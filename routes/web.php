<?php

use Illuminate\Support\Facades\Route; 

Route::get('/', 'AuthController@showFormLogin')->name('login');
Route::get('login', 'AuthController@showFormLogin')->name('login');
Route::post('login', 'AuthController@login');
Route::get('register', 'AuthController@showFormRegister')->name('register');
Route::post('register', 'AuthController@register');
// Route::post('register', 'RegisterController@create');
Route::get('user/pelanggaran/poin/','PelanggaranC@cekpoin');

Auth::routes();
Auth::routes(['verify' => true]);
// Route::group(['middleware' => 'auth'], function () {
Route::get('data/prodi/{id}','DataUserController@prodi');
Route::get('logout', 'AuthController@logout')->name('logout');

Route::get('home', 'HomeController@index')->name('home');
Route::get('user/update/{id}','HomeController@view_update');
Route::patch('user/update','HomeController@update');
Route::patch('user/insert','HomeController@insert');
Route::get('user/delete/{id}','HomeController@delete');
Route::get('dashboard','HomeController@dash')->name('dashboard');


Route::get('user/data','DataUserController@index');

	// get data indonesia

Route::get('id/kabupaten/{id}','DataUserController@add_ajax_kab');
Route::get('id/kecamatan/{id}','DataUserController@add_ajax_kec');
Route::get('id/desa/{id}','DataUserController@add_ajax_des');

Route::get('user/detail/{id}','DataUserController@detail');
Route::get('user/update/{id}','DataUserController@edit_view');
Route::get('user/data/delete/{id}','DataUserController@delete');

Route::post('user/data/insert1','DataUserController@dataTaruna');
Route::post('user/data/insert2','DataUserController@dataOrtu');
Route::post('user/data/insert3','DataUserController@dataMediaTaruna');

Route::post('user/data/{id}/update1','DataUserController@updateTarura');
Route::post('user/data/update2','DataUserController@updateOrtu');
Route::post('user/data/update3','DataUserController@updateMedia');


	// pelanggaran
Route::get('user/pelanggaran/{tingkat}','PelanggaranC@index');
Route::get('user/pelanggaran/jenis/{data}','PelanggaranC@jenis_pel');
Route::post('user/pelanggaran/insert','PelanggaranC@insert');
Route::post('user/pelanggaran/ins','PelanggaranC@insert');
Route::get('user/pelanggaran/detail/{id}','PelanggaranC@show');
Route::get('user/pelanggaran/delete/{id}','PelanggaranC@delete');

Route::get('/kirimemail/{id}','HomeController@sendEmail');
Route::get('wali','DataUserController@kelas');
Route::get('downloadExcel/{id}','PelanggaranC@DownloadExcel');

Route::get('taruna/{id}','PrestasiController@home');
Route::get('taruna/jasmaniJson/{id}','PrestasiController@jasmaniJson');
Route::get('taruna/prestasi/{id}','PrestasiController@getPrestasi');
Route::get('taruna/prestasi/det/{id}','PrestasiController@getDetPrestasi');
Route::get('taruna/prestasi/delete/{id}','PrestasiController@delete');
Route::post('taruna/prestasi/insert','PrestasiController@insert');
Route::post('taruna/prestasi/update','PrestasiController@update');
Route::get('taruna/prestasi/get/{id}','PrestasiController@getData');
Route::get('taruna/pelanggaran/get/{id}','PelanggaranC@getData');
Route::post('taruna/pelanggaran/update','PelanggaranC@updatePel');
Route::get('getkelas','PelanggaranC@kelas');
// jasmani
Route::post('taruna/jasmani/insert','PrestasiController@insertJasmani');
Route::post('taruna/jasmani/hapus','PrestasiController@hapusJasmani');
Route::post('taruna/jasmani/update','PrestasiController@updateJasmani');
Route::get('taruna/getjasmani/{id}','PrestasiController@getJasmani');



// WILAYAH
Route::get('provinsi','DaerahController@provinsi');
Route::get('kabupaten/{id}','DaerahController@kabupaten');
Route::get('kecamatan/{id}','DaerahController@kecamatan');
Route::get('desa/{id}','DaerahController@desa');
Route::get('ortu/{id}','DaerahController@daerahOrtu');

// MASTER
Route::get('prestasi','MasterController@indexprestasi');
Route::get('prestasi/html','MasterController@loadHtml');
Route::get('prestasi/delete/{id}','MasterController@deletePrestasi');
Route::get('prestasi/view/{id}','MasterController@viewUpdatePrestasi');
Route::post('prestasi/insert','MasterController@insertPrestasi');
Route::post('prestasi/update','MasterController@updatePrestasi');

Route::get('pelanggaran','MasterController@indexPelanggaran');
Route::get('pelanggaran/html','MasterController@loadHtmlPel');
Route::get('pelanggaran/view/{id}','MasterController@viewUpdatePelanggaran');
Route::post('pelanggaran/update','MasterController@updatePelanggaran');
Route::post('pelanggaran/insert','MasterController@insertPelanggaran');
Route::get('pelanggaran/delete/{id}','MasterController@deletePelanggaran');
Route::get('pdf/','MasterController@indexPDF');
Route::get('pdf/preview','MasterController@previewPDF');
Route::post('pdf/update','MasterController@updatePDF');


Route::get('prodi','prodiController@index');
Route::get('prodi/json','prodiController@json');
Route::get('prodi/detail/{id}','prodiController@detProdi');
Route::post('prodi/insert','prodiController@insert');
Route::post('prodi/update','prodiController@update');
Route::post('prodi/hapus','prodiController@hapus');

Route::get('kelas','controllerKelas@index');
Route::get('kelas/json','controllerKelas@json');
Route::get('kelas/detail/{id}','controllerKelas@detail');
Route::post('kelas/insert','controllerKelas@insert');
Route::post('kelas/update','controllerKelas@update');
Route::post('kelas/hapus','controllerKelas@hapus');
Route::patch('acc/update','AuthController@updatepass');


Route::post('absensi/insert','AbsensiController@insert');
Route::post('absensi/update','AbsensiController@update');
Route::post('absensi/hapus','AbsensiController@hapus');
Route::get('absensi/json/{id}','AbsensiController@show');
Route::get('absensi/detail/{id}','AbsensiController@detail');

// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
